<?php

/**
 * @author    syafiq <syafiq@my-sands.com>
 * @since     1.0.0 build 4b7372
 * @version   1.1.0 build 43ec40
 * @copyright 2014 syafiq
 * @filesource
 */
/**
 * subdistrict - mukim.
 * sddsa: Kategori Data > Data Generik > Organisasi > Kod Bandar
 */
$items = [
    /*
     * perlis
     * note: sddsa letak 0900 disebabkan perlis tiada daerah 
     *       tapi akan buat masalah lain dalam pilihan daerah maka list akan teruskan pada 0901
     */
    ['id' => '090101', 'name' => 'ABI', 'district_id' => '0901'],
    ['id' => '090102', 'name' => 'ARAU', 'district_id' => '0901'],
    ['id' => '090103', 'name' => 'BERSERI', 'district_id' => '0901'],
    ['id' => '090104', 'name' => 'CHUPING', 'district_id' => '0901'],
    ['id' => '090105', 'name' => 'UTAN AJI', 'district_id' => '0901'],
    ['id' => '090106', 'name' => 'JEJAWI', 'district_id' => '0901'],
    ['id' => '090107', 'name' => 'KAYANG', 'district_id' => '0901'],
    ['id' => '090108', 'name' => 'KECHOR', 'district_id' => '0901'],
    ['id' => '090109', 'name' => 'KUALA PERLIS', 'district_id' => '0901'],
    ['id' => '090110', 'name' => 'KURONG ANAI', 'district_id' => '0901'],
    ['id' => '090111', 'name' => 'KURONG BATANG', 'district_id' => '0901'],
    ['id' => '090112', 'name' => 'NGOLANG', 'district_id' => '0901'],
    ['id' => '090113', 'name' => 'ORAN', 'district_id' => '0901'],
    ['id' => '090114', 'name' => 'PADANG PAUH', 'district_id' => '0901'],
    ['id' => '090115', 'name' => 'PADANG SIDING', 'district_id' => '0901'],
    ['id' => '090116', 'name' => 'PAYA', 'district_id' => '0901'],
    ['id' => '090117', 'name' => 'SANGLANG', 'district_id' => '0901'],
    ['id' => '090118', 'name' => 'SENA', 'district_id' => '0901'],
    ['id' => '090119', 'name' => 'SERIAP', 'district_id' => '0901'],
    ['id' => '090120', 'name' => 'SUNGAI ADAM', 'district_id' => '0901'],
    ['id' => '090121', 'name' => 'TITI TINGGI', 'district_id' => '0901'],
    ['id' => '090122', 'name' => 'WANG BINTONG', 'district_id' => '0901'],

    ['id' => '090140', 'name' => 'BANDAR ARAU', 'district_id' => '0901'],
    ['id' => '090141', 'name' => 'BANDAR KANGAR', 'district_id' => '0901'],

    ['id' => '090170', 'name' => 'PEKAN KUALA PERLIS', 'district_id' => '0901'],
    ['id' => '090171', 'name' => 'PEKAN KAKI BUKIT', 'district_id' => '0901'],
    /*
     * kedah
     */
    /*
     * === KOTA SETAR
     */
    ['id' => '020101', 'name' => 'ALOR MALAI', 'district_id' => '0201'],
    // ['id' => '020102', 'name' => 'ALOR MERAH', 'district_id' => '0201'], // deprecated?
    ['id' => '020103', 'name' => 'ANAK BUKIT', 'district_id' => '0201'],
    // ['id' => '020104', 'name' => 'BUKIT LADA', 'district_id' => '0201'], // tukar ke 021206
    // ['id' => '020105', 'name' => 'BUKIT PINANG', 'district_id' => '0201'], // deprecated?
    // ['id' => '020106', 'name' => 'DERANG', 'district_id' => '0201'], // tukar ke 021201
    ['id' => '020107', 'name' => 'DERGA', 'district_id' => '0201'],
    // ['id' => '020108', 'name' => 'GAJAH MATI', 'district_id' => '0201'], // deprecated?
    ['id' => '020109', 'name' => 'GUNONG', 'district_id' => '0201'],
    // ['id' => '020110', 'name' => 'HUTAN KAMPONG', 'district_id' => '0201'], // deprecated?
    // ['id' => '020111', 'name' => 'JABI', 'district_id' => '0201'], // deprecated?
    ['id' => '020112', 'name' => 'KANGKONG', 'district_id' => '0201'],
    // ['id' => '020113', 'name' => 'KOTA SETAR', 'district_id' => '0201'], // deprecated?
    // ['id' => '020114', 'name' => 'KUALA KEDAH', 'district_id' => '0201'], // tukar ke bandar:020142?
    ['id' => '020115', 'name' => 'KUBANG ROTAN', 'district_id' => '0201'],
    ['id' => '020116', 'name' => 'LANGGAR', 'district_id' => '0201'],
    ['id' => '020117', 'name' => 'LENGKUAS', 'district_id' => '0201'],
    ['id' => '020118', 'name' => 'LEPAI', 'district_id' => '0201'],
    // ['id' => '020119', 'name' => 'LESONG', 'district_id' => '0201'], // tukar ke 021202
    ['id' => '020120', 'name' => 'LIMBONG', 'district_id' => '0201'],
    // ['id' => '020121', 'name' => 'MERGONG', 'district_id' => '0201'], // deprecated?
    ['id' => '020122', 'name' => 'PADANG HANG', 'district_id' => '0201'],
    ['id' => '020123', 'name' => 'PADANG LALANG', 'district_id' => '0201'],
    ['id' => '020124', 'name' => 'PENGKALAN KUNDOR', 'district_id' => '0201'],
    // ['id' => '020125', 'name' => 'PUMPONG', 'district_id' => '0201'], // deprecated?
    ['id' => '020126', 'name' => 'SALA KECHIL', 'district_id' => '0201'],
    ['id' => '020127', 'name' => 'SUNGAI BAHARU', 'district_id' => '0201'],
    ['id' => '020128', 'name' => 'TAJAR', 'district_id' => '0201'],
    ['id' => '020129', 'name' => 'TEBENGAU', 'district_id' => '0201'],
    ['id' => '020130', 'name' => 'TELAGA MAS', 'district_id' => '0201'],
    ['id' => '020131', 'name' => 'TELOK CHENGAI', 'district_id' => '0201'],
    // ['id' => '020132', 'name' => 'TELOK KECHAI', 'district_id' => '0201'], // deprecated?
    // ['id' => '020133', 'name' => 'TITI GAJAH', 'district_id' => '0201'], // deprecated?
    // ['id' => '020134', 'name' => 'TUALANG', 'district_id' => '0201'], // tukar ke 021203
    
    ['id' => '020140', 'name' => 'BANDAR ALOR SETAR', 'district_id' => '0201'],
    ['id' => '020141', 'name' => 'BANDAR ANAK BUKIT', 'district_id' => '0201'],
    ['id' => '020143', 'name' => 'BANDAR KUALA KEDAH', 'district_id' => '0201'],
    ['id' => '020147', 'name' => 'BANDAR ALOR MERAH', 'district_id' => '0201'],
    ['id' => '020148', 'name' => 'BANDAR BUKIT PINANG', 'district_id' => '0201'],
    ['id' => '020149', 'name' => 'BANDAR LANGGAR', 'district_id' => '0201'],
    ['id' => '020150', 'name' => 'BANDAR SIMPANG EMPAT', 'district_id' => '0201'],

    ['id' => '020170', 'name' => 'PEKAN ALOR JANGGUS', 'district_id' => '0201'],
    ['id' => '020172', 'name' => 'PEKAN KOTA SARANG SEMUT', 'district_id' => '0201'],
    ['id' => '020175', 'name' => 'PEKAN GUNUNG', 'district_id' => '0201'],
    /*
     * === KUBANG PASU
     */
    ['id' => '020201', 'name' => 'AH', 'district_id' => '0202'],
    ['id' => '020202', 'name' => 'BINJAL', 'district_id' => '0202'],
    ['id' => '020203', 'name' => 'GELONG', 'district_id' => '0202'],
    ['id' => '020204', 'name' => 'BUKIT TINGGI', 'district_id' => '0202'],
    ['id' => '020205', 'name' => 'HUSBA', 'district_id' => '0202'],
    ['id' => '020206', 'name' => 'JERAM', 'district_id' => '0202'],
    ['id' => '020207', 'name' => 'JERLUN', 'district_id' => '0202'],
    ['id' => '020208', 'name' => 'JITRA', 'district_id' => '0202'],
    ['id' => '020209', 'name' => 'KEPELU', 'district_id' => '0202'],
    ['id' => '020210', 'name' => 'KUBANG PASU', 'district_id' => '0202'],
    ['id' => '020211', 'name' => 'MALAU', 'district_id' => '0202'],
    ['id' => '020212', 'name' => 'NAGA', 'district_id' => '0202'],
    ['id' => '020213', 'name' => 'PADANG PERAHU', 'district_id' => '0202'],
    ['id' => '020214', 'name' => 'PELUBANG', 'district_id' => '0202'],
    ['id' => '020215', 'name' => 'PERING', 'district_id' => '0202'],
    ['id' => '020216', 'name' => 'PUTAT', 'district_id' => '0202'],
    ['id' => '020217', 'name' => 'SANGLANG', 'district_id' => '0202'],
    ['id' => '020218', 'name' => 'SUNGAI LAKA', 'district_id' => '0202'],
    ['id' => '020219', 'name' => 'TEMIN', 'district_id' => '0202'],
    ['id' => '020220', 'name' => 'TUNJANG', 'district_id' => '0202'],
    ['id' => '020221', 'name' => 'WANG TEPUS', 'district_id' => '0202'],

    ['id' => '020240', 'name' => 'BANDAR CHANGLUN', 'district_id' => '0202'],
    ['id' => '020241', 'name' => 'BANDAR JITRA', 'district_id' => '0202'],
    ['id' => '020242', 'name' => 'BANDAR KODIANG', 'district_id' => '0202'],
    ['id' => '020243', 'name' => 'BANDAR TUNJANG', 'district_id' => '0202'],
    ['id' => '020244', 'name' => 'BANDAR BANDAR DARULAMAN', 'district_id' => '0202'],
    ['id' => '020245', 'name' => 'BANDAR PADANG SERA', 'district_id' => '0202'],
    ['id' => '020246', 'name' => 'BANDAR KEPALA BATAS', 'district_id' => '0202'],
    ['id' => '020247', 'name' => 'BANDAR BUKIT KAYU HITAM', 'district_id' => '0202'],

    ['id' => '020270', 'name' => 'PEKAN AYER HITAM', 'district_id' => '0202'],
    ['id' => '020276', 'name' => 'PEKAN KUALA SANGLANG', 'district_id' => '0202'],
    ['id' => '020281', 'name' => 'PEKAN SANGLANG', 'district_id' => '0202'],
    ['id' => '020289', 'name' => 'PEKAN KERPAN', 'district_id' => '0202'],
    ['id' => '020290', 'name' => 'PEKAN SINTOK', 'district_id' => '0202'],
    ['id' => '020291', 'name' => 'PEKAN NAPOH', 'district_id' => '0202'],
    ['id' => '020292', 'name' => 'PEKAN SUNGAI KOROK', 'district_id' => '0202'],
    /*
     * === PADANG TERAP
     */
    ['id' => '020302', 'name' => 'BATANG TUNGGANG KANAN', 'district_id' => '0203'],
    ['id' => '020301', 'name' => 'BATANG TUNGGANG KIRI', 'district_id' => '0203'],
    ['id' => '020303', 'name' => 'BELIMBING KANAN', 'district_id' => '0203'],
    ['id' => '020304', 'name' => 'BELIMBING KIRI', 'district_id' => '0203'],
    ['id' => '020305', 'name' => 'KURONG HITAM', 'district_id' => '0203'],
    ['id' => '020306', 'name' => 'PADANG TERNAK', 'district_id' => '0203'],
    ['id' => '020307', 'name' => 'PADANG TERAP KANAN', 'district_id' => '0203'],
    ['id' => '020308', 'name' => 'PADANG TERAP KIRI', 'district_id' => '0203'],
    ['id' => '020309', 'name' => 'PEDU', 'district_id' => '0203'],
    ['id' => '020310', 'name' => 'TEKAI', 'district_id' => '0203'],
    ['id' => '020311', 'name' => 'TOLAK', 'district_id' => '0203'],

    ['id' => '020340', 'name' => 'BANDAR KUALA NERANG', 'district_id' => '0203'],

    ['id' => '020370', 'name' => 'PEKAN NAKA', 'district_id' => '0203'],
    ['id' => '020371', 'name' => 'PEKAN DURIAN BURUNG', 'district_id' => '0203'],
    ['id' => '020372', 'name' => 'PEKAN LUBOK MERBAU', 'district_id' => '0203'],
    ['id' => '020373', 'name' => 'PEKAN BUKIT TEMBAGA', 'district_id' => '0203'],
    ['id' => '020374', 'name' => 'PEKAN PADANG SANAI', 'district_id' => '0203'],
    ['id' => '020375', 'name' => 'PEKAN KAMPUNG TANJUNG', 'district_id' => '0203'],
    /*
     * === LANGKAWI
     */
    ['id' => '020402', 'name' => 'AYER HANGAT', 'district_id' => '0204'],
    ['id' => '020401', 'name' => 'BOHOR', 'district_id' => '0204'],
    ['id' => '020403', 'name' => 'KEDAWANG', 'district_id' => '0204'],
    ['id' => '020404', 'name' => 'KUAH', 'district_id' => '0204'],
    ['id' => '020405', 'name' => 'PADANG MASIRAT', 'district_id' => '0204'],
    ['id' => '020406', 'name' => 'ULU MELAKA', 'district_id' => '0204'],

    ['id' => '020440', 'name' => 'BANDAR KUAH', 'district_id' => '0204'],
    ['id' => '020442', 'name' => 'BANDAR PADANG LALANG', 'district_id' => '0204'],

    ['id' => '020472', 'name' => 'PEKAN TOK DATAI', 'district_id' => '0204'],
    /*
     * === KUALA MUDA
     */
    ['id' => '020501', 'name' => 'BUJANG', 'district_id' => '0205'],
    ['id' => '020502', 'name' => 'BUKIT MERIAM', 'district_id' => '0205'],
    ['id' => '020503', 'name' => 'GURUN', 'district_id' => '0205'],
    ['id' => '020504', 'name' => 'HAJI KUDONG', 'district_id' => '0205'],
    ['id' => '020505', 'name' => 'KOTA', 'district_id' => '0205'],
    ['id' => '020506', 'name' => 'KUALA', 'district_id' => '0205'],
    ['id' => '020507', 'name' => 'MERBOK', 'district_id' => '0205'],
    ['id' => '020508', 'name' => 'PEKULA', 'district_id' => '0205'],
    ['id' => '020509', 'name' => 'PINANG TUNGGAL', 'district_id' => '0205'],
    ['id' => '020510', 'name' => 'RANTAU PANJANG', 'district_id' => '0205'],
    ['id' => '020511', 'name' => 'SEMELING', 'district_id' => '0205'],
    ['id' => '020512', 'name' => 'SIDAM KIRI', 'district_id' => '0205'],
    ['id' => '020513', 'name' => 'SIMPOR', 'district_id' => '0205'],
    ['id' => '020514', 'name' => 'SUNGAI PASIR', 'district_id' => '0205'],
    ['id' => '020515', 'name' => 'SUNGAI PETANI', 'district_id' => '0205'],
    ['id' => '020516', 'name' => 'TELOI KIRI', 'district_id' => '0205'],
    /*
     * === YAN
     */
    ['id' => '020602', 'name' => 'DULANG', 'district_id' => '0206'],
    ['id' => '020601', 'name' => 'SALA BESAR', 'district_id' => '0206'],
    ['id' => '020603', 'name' => 'SINGKIR', 'district_id' => '0206'],
    ['id' => '020604', 'name' => 'SUNGAI DAUN', 'district_id' => '0206'],
    ['id' => '020605', 'name' => 'YAN', 'district_id' => '0206'],

    ['id' => '020640', 'name' => 'BANDAR GUAR CHEMPEDAK', 'district_id' => '0206'],
    ['id' => '020641', 'name' => 'BANDAR YAN', 'district_id' => '0206'],

    ['id' => '020671', 'name' => 'PEKAN SIMPANG TIGA SUNGAI LIMAU', 'district_id' => '0206'],
    ['id' => '020672', 'name' => 'PEKAN SUNGAI LIMAU DALAM', 'district_id' => '0206'],
    ['id' => '020674', 'name' => 'PEKAN TEROI', 'district_id' => '0206'],
    ['id' => '020675', 'name' => 'PEKAN SINGKIR', 'district_id' => '0206'],
    /*
     * === SIK
     */
    ['id' => '020902', 'name' => 'JENERI', 'district_id' => '0209'],
    ['id' => '020901', 'name' => 'SIK', 'district_id' => '0209'],
    ['id' => '020903', 'name' => 'SOK', 'district_id' => '0209'],

    ['id' => '020940', 'name' => 'BANDAR SIK', 'district_id' => '0209'],

    ['id' => '020970', 'name' => 'PEKAN BATU LAMA SIK', 'district_id' => '0209'],
    ['id' => '020973', 'name' => 'PEKAN GULAU', 'district_id' => '0209'],
    ['id' => '020974', 'name' => 'PEKAN GAJAH PUTEH', 'district_id' => '0209'],
    ['id' => '020975', 'name' => 'PEKAN CHAROK PADANG', 'district_id' => '0209'],
    /*
     * === BALING
     */
    ['id' => '020801', 'name' => 'BAKAI', 'district_id' => '0208'],
    ['id' => '020802', 'name' => 'BALING', 'district_id' => '0208'],
    ['id' => '020803', 'name' => 'BONGOR', 'district_id' => '0208'],
    ['id' => '020804', 'name' => 'KUPANG', 'district_id' => '0208'],
    ['id' => '020805', 'name' => 'PULAI', 'district_id' => '0208'],
    ['id' => '020806', 'name' => 'SIONG', 'district_id' => '0208'],
    ['id' => '020807', 'name' => 'TAWAR', 'district_id' => '0208'],
    ['id' => '020808', 'name' => 'TELOI KANAN', 'district_id' => '0208'],

    ['id' => '020840', 'name' => 'BANDAR BALING', 'district_id' => '0208'],
    ['id' => '020841', 'name' => 'BANDAR KUALA KETIL', 'district_id' => '0208'],
    ['id' => '020842', 'name' => 'BANDAR KUPANG', 'district_id' => '0208'],

    ['id' => '020872', 'name' => 'PEKAN KAMPUNG BARU KEJAI', 'district_id' => '0208'],
    ['id' => '020874', 'name' => 'PEKAN PULAI', 'district_id' => '0208'],
    ['id' => '020875', 'name' => 'PEKAN TAWAR', 'district_id' => '0208'],
    ['id' => '020876', 'name' => 'PEKAN KUALA PEGANG', 'district_id' => '0208'],
    ['id' => '020878', 'name' => 'PEKAN PARIT PANJANG', 'district_id' => '0208'],
    ['id' => '020879', 'name' => 'PEKAN KAMPUNG LALANG', 'district_id' => '0208'],
    ['id' => '020880', 'name' => 'PEKAN MALAU', 'district_id' => '0208'],
    /*
     * === KULIM
     */
    ['id' => '020902', 'name' => 'BAGAN SENA', 'district_id' => '0209'],
    ['id' => '020901', 'name' => 'JUNJONG', 'district_id' => '0209'],
    ['id' => '020903', 'name' => 'KARANGAN', 'district_id' => '0209'],
    // ['id' => '020904', 'name' => 'KELADI', 'district_id' => '0209'], // deprecated?
    ['id' => '020905', 'name' => 'KULIM', 'district_id' => '0209'],
    ['id' => '020906', 'name' => 'LUNAS', 'district_id' => '0209'],
    ['id' => '020907', 'name' => 'MAHANG', 'district_id' => '0209'],
    ['id' => '020908', 'name' => 'NAGA LILIT', 'district_id' => '0209'],
    ['id' => '020909', 'name' => 'PADANG CHINA', 'district_id' => '0209'],
    ['id' => '020910', 'name' => 'PADANG MEHA', 'district_id' => '0209'],
    ['id' => '020911', 'name' => 'SEDIM', 'district_id' => '0209'],
    ['id' => '020912', 'name' => 'SIDAM KANAN', 'district_id' => '0209'],
    ['id' => '020913', 'name' => 'SUNGAI SELUANG', 'district_id' => '0209'],
    ['id' => '020914', 'name' => 'SUNGAI ULAR', 'district_id' => '0209'],
    ['id' => '020915', 'name' => 'TERAP', 'district_id' => '0209'],

    ['id' => '020940', 'name' => 'BANDAR KULIM', 'district_id' => '0209'],
    ['id' => '020941', 'name' => 'BANDAR LUNAS', 'district_id' => '0209'],
    ['id' => '020942', 'name' => 'BANDAR PADANG SERAI', 'district_id' => '0209'],

    ['id' => '020970', 'name' => 'PEKAN JUNJONG', 'district_id' => '0209'],
    ['id' => '020971', 'name' => 'PEKAN KARANGAN', 'district_id' => '0209'],
    ['id' => '020972', 'name' => 'PEKAN LABU BESAR', 'district_id' => '0209'],
    ['id' => '020973', 'name' => 'PEKAN MAHANG', 'district_id' => '0209'],
    ['id' => '020974', 'name' => 'PEKAN MERBAU PULAS', 'district_id' => '0209'],
    ['id' => '020975', 'name' => 'PEKAN SUNGAI KARANGAN', 'district_id' => '0209'],
    ['id' => '020976', 'name' => 'PEKAN SUNGAI KOB', 'district_id' => '0209'],
    ['id' => '020977', 'name' => 'PEKAN PADANG MEHA', 'district_id' => '0209'],
    /*
     * === BANDAR BAHARU
     */
    ['id' => '021001', 'name' => 'BAGAN SEMAK', 'district_id' => '0210'],
    ['id' => '021002', 'name' => 'KUALA SELAMA', 'district_id' => '0210'],
    ['id' => '021003', 'name' => 'PERMATANG PASIR', 'district_id' => '0210'],
    ['id' => '021004', 'name' => 'RELAU', 'district_id' => '0210'],
    ['id' => '021005', 'name' => 'SERDANG', 'district_id' => '0210'],
    ['id' => '021006', 'name' => 'SUNGAI BATU', 'district_id' => '0210'],
    ['id' => '021007', 'name' => 'SUNGAI KECHIL', 'district_id' => '0210'],

    ['id' => '021040', 'name' => 'BANDAR BANDAR BAHARU', 'district_id' => '0210'],
    ['id' => '021041', 'name' => 'BANDAR SERDANG', 'district_id' => '0210'],

    ['id' => '021070', 'name' => 'PEKAN LUBUK BUNTAR', 'district_id' => '0210'],
    ['id' => '021071', 'name' => 'PEKAN SELAMA', 'district_id' => '0210'],
    ['id' => '021072', 'name' => 'PEKAN SUNGAI KECIL ILLIR', 'district_id' => '0210'],
    ['id' => '021073', 'name' => 'PEKAN RELAU', 'district_id' => '0210'],
    /*
     * === PENDANG
     */
    ['id' => '021102', 'name' => 'AYER PUTEH', 'district_id' => '0211'],
    ['id' => '021101', 'name' => 'BUKIT RAYA', 'district_id' => '0211'],
    ['id' => '021103', 'name' => 'GUAR KEPAYANG', 'district_id' => '0211'],
    ['id' => '021104', 'name' => 'PADANG KERBAU', 'district_id' => '0211'],
    ['id' => '021105', 'name' => 'PADANG PELIANG', 'district_id' => '0211'],
    ['id' => '021106', 'name' => 'PADANG PUSING', 'district_id' => '0211'],
    // ['id' => '021107', 'name' => 'RAMBAI', 'district_id' => '0211'], // deprecated?
    ['id' => '021108', 'name' => 'TOBIAR', 'district_id' => '0211'],

    ['id' => '021140', 'name' => 'BANDAR PEDANG', 'district_id' => '0211'],

    ['id' => '021170', 'name' => 'PEKAN BUKIT JENUN', 'district_id' => '0211'],
    ['id' => '021171', 'name' => 'PEKAN KUBUR PANJANG', 'district_id' => '0211'],
    ['id' => '021172', 'name' => 'PEKAN TANAH MERAH', 'district_id' => '0211'],
    ['id' => '021173', 'name' => 'PEKAN TOKAI', 'district_id' => '0211'],
    ['id' => '021174', 'name' => 'PEKAN KOBAH', 'district_id' => '0211'],
    ['id' => '021175', 'name' => 'PEKAN KAMPUNG BARU', 'district_id' => '0211'],
    ['id' => '021176', 'name' => 'PEKAN SUNGAI TIANG', 'district_id' => '0211'],
    /*
     * === POKOK SENA
     */
    ['id' => '021201', 'name' => 'DERANG', 'district_id' => '0212'],
    ['id' => '021202', 'name' => 'LESONG', 'district_id' => '0212'],
    ['id' => '021203', 'name' => 'TUALANG', 'district_id' => '0212'],
    ['id' => '021206', 'name' => 'BUKIT LADA', 'district_id' => '0212'],

    ['id' => '021240', 'name' => 'BANDAR POKOK SENA', 'district_id' => '0212'],

    ['id' => '021270', 'name' => 'PEKAN KEBUN 500', 'district_id' => '0212'],
    /*
     * kelantan
     */
    /*
     * === BACHOK
     */
    ['id' => '030102', 'name' => 'BEKLAM', 'district_id' => '0301'],
    ['id' => '030101', 'name' => 'GUNONG (GUNONG TIMOR)', 'district_id' => '0301'],
    ['id' => '030103', 'name' => 'MAHLIGAI', 'district_id' => '0301'],
    ['id' => '030104', 'name' => 'PERUPOK', 'district_id' => '0301'],
    ['id' => '030105', 'name' => 'MELAWI (REPEK)', 'district_id' => '0301'],
    ['id' => '030106', 'name' => 'TAWANG (MENTUAN)', 'district_id' => '0301'],
    ['id' => '030107', 'name' => 'TELONG', 'district_id' => '0301'],
    ['id' => '030108', 'name' => 'TANJUNG PAUH', 'district_id' => '0301'],
    /*
     * === KOTA BHARU
     */
    ['id' => '030202', 'name' => 'BADANG', 'district_id' => '0302'],
    ['id' => '030201', 'name' => 'BANGGU', 'district_id' => '0302'],
    ['id' => '030203', 'name' => 'BETA', 'district_id' => '0302'],
    ['id' => '030204', 'name' => 'KADOK', 'district_id' => '0302'],
    ['id' => '030205', 'name' => 'KEMUMIN', 'district_id' => '0302'],
    ['id' => '030206', 'name' => 'KOTA', 'district_id' => '0302'],
    ['id' => '030207', 'name' => 'LIMBAT', 'district_id' => '0302'],
    ['id' => '030208', 'name' => 'KUBANG KERIAN (LUNDANG)', 'district_id' => '0302'],
    ['id' => '030209', 'name' => 'KETEREH (PANGKAL KALONG)', 'district_id' => '0302'],
    ['id' => '030210', 'name' => 'PANJI', 'district_id' => '0302'],
    ['id' => '030211', 'name' => 'PENDEK', 'district_id' => '0302'],
    ['id' => '030212', 'name' => 'PERINGAT', 'district_id' => '0302'],
    ['id' => '030213', 'name' => 'SALOR', 'district_id' => '0302'],
    ['id' => '030214', 'name' => 'SERING', 'district_id' => '0302'],
    ['id' => '030215', 'name' => 'KOTA BHARU', 'district_id' => '0302'],
    /*
     * === MACHANG
     */
    ['id' => '030302', 'name' => 'LABOK', 'district_id' => '0303'],
    ['id' => '030301', 'name' => 'PANYIT', 'district_id' => '0303'],
    ['id' => '030303', 'name' => 'PULAI CHONDONG', 'district_id' => '0303'],
    ['id' => '030304', 'name' => 'PANGKAL MELERET', 'district_id' => '0303'],
    ['id' => '030305', 'name' => 'TEMANGAN', 'district_id' => '0303'],
    ['id' => '030306', 'name' => 'ULU SAT', 'district_id' => '0303'],
    /*
     * === PASIR MAS
     */
    ['id' => '030402', 'name' => 'ALOR PASIR', 'district_id' => '0304'],
    ['id' => '030401', 'name' => 'BUNUT SUSU', 'district_id' => '0304'],
    ['id' => '030403', 'name' => 'CHETOK', 'district_id' => '0304'],
    ['id' => '030404', 'name' => 'GUAL PERIOK', 'district_id' => '0304'],
    ['id' => '030405', 'name' => 'KANGKONG', 'district_id' => '0304'],
    ['id' => '030406', 'name' => 'KUALA LEMAL', 'district_id' => '0304'],
    ['id' => '030407', 'name' => 'KUBANG GADONG', 'district_id' => '0304'],
    ['id' => '030408', 'name' => 'KUBANG SEPAT', 'district_id' => '0304'],
    ['id' => '030409', 'name' => 'PASIR MAS', 'district_id' => '0304'],
    ['id' => '030410', 'name' => 'RANTAU PANJANG', 'district_id' => '0304'],
    /*
     * === PASIR PUTEH
     */
    ['id' => '030502', 'name' => 'PADANG PAK AMAT', 'district_id' => '0305'],
    ['id' => '030501', 'name' => 'BUKIT ABAL', 'district_id' => '0305'],
    ['id' => '030503', 'name' => 'BUKIT AWANG', 'district_id' => '0305'],
    ['id' => '030504', 'name' => 'BUKIT JAWA', 'district_id' => '0305'],
    ['id' => '030505', 'name' => 'GONG DATOK', 'district_id' => '0305'],
    ['id' => '030506', 'name' => 'JERAM', 'district_id' => '0305'],
    ['id' => '030507', 'name' => 'LIMBONGAN', 'district_id' => '0305'],
    ['id' => '030508', 'name' => 'SEMERAK', 'district_id' => '0305'],
    /*
     * === TANAH MERAH
     */
    ['id' => '030602', 'name' => 'KUSIAL', 'district_id' => '0306'],
    ['id' => '030601', 'name' => 'JEDOK', 'district_id' => '0306'],
    ['id' => '030603', 'name' => 'ULU KUSIAL', 'district_id' => '0306'],
    /*
     * === TUMPAT
     */
    ['id' => '030702', 'name' => 'JAL BESAR', 'district_id' => '0307'],
    ['id' => '030701', 'name' => 'KEBAKAT', 'district_id' => '0307'],
    ['id' => '030703', 'name' => 'PENGKALAN KUBOR', 'district_id' => '0307'],
    ['id' => '030704', 'name' => 'SUNGAI PINANG', 'district_id' => '0307'],
    ['id' => '030705', 'name' => 'TERBOK', 'district_id' => '0307'],
    ['id' => '030706', 'name' => 'TUMBAT', 'district_id' => '0307'],
    ['id' => '030707', 'name' => 'WAKAF BHARU', 'district_id' => '0307'],
    /*
     * === GUA MUSANG
     */
    ['id' => '030802', 'name' => 'GALAS', 'district_id' => '0308'],
    ['id' => '030801', 'name' => 'BERTAM', 'district_id' => '0308'],
    ['id' => '030803', 'name' => 'CHIKU', 'district_id' => '0308'],
    /*
     * === KUALA KRAI
     */
    ['id' => '030902', 'name' => 'BATU MENGKEBANG', 'district_id' => '0309'],
    ['id' => '030901', 'name' => 'OLAK JERAM', 'district_id' => '0309'],
    ['id' => '030903', 'name' => 'DABONG', 'district_id' => '0309'],
    /*
     * === JELI
     */
    ['id' => '031002', 'name' => 'BATU MELINTANG (BELIMBING)', 'district_id' => '0310'],
    ['id' => '031001', 'name' => 'JELI', 'district_id' => '0310'],
    ['id' => '031003', 'name' => 'KUALA BALAH', 'district_id' => '0310'],
    /*
     * terengganu
     */
    /*
     * === BESUT
     */
    ['id' => '110102', 'name' => 'BUKIT KENAK', 'district_id' => '1101'],
    ['id' => '110101', 'name' => 'BUKIT PUTERI', 'district_id' => '1101'],
    ['id' => '110103', 'name' => 'JABI', 'district_id' => '1101'],
    ['id' => '110104', 'name' => 'KAMPONG RAJA', 'district_id' => '1101'],
    ['id' => '110105', 'name' => 'KELUANG', 'district_id' => '1101'],
    ['id' => '110106', 'name' => 'KERANDANG', 'district_id' => '1101'],
    ['id' => '110107', 'name' => 'KUALA BESUT', 'district_id' => '1101'],
    ['id' => '110108', 'name' => 'KUBANG BEMBAN', 'district_id' => '1101'],
    ['id' => '110109', 'name' => 'LUBOK KAWAH', 'district_id' => '1101'],
    ['id' => '110110', 'name' => 'PASIR AKAR', 'district_id' => '1101'],
    ['id' => '110111', 'name' => 'PELAGAT', 'district_id' => '1101'],
    ['id' => '110112', 'name' => 'PENGKALAN NANGKA', 'district_id' => '1101'],
    ['id' => '110113', 'name' => 'PULAU PERHENTIAN', 'district_id' => '1101'],
    ['id' => '110114', 'name' => 'TEMBILA', 'district_id' => '1101'],
    ['id' => '110115', 'name' => 'TENANG', 'district_id' => '1101'],
    ['id' => '110116', 'name' => 'HULU BESUT', 'district_id' => '1101'],
    /*
     * === DUNGUN
     */
    ['id' => '110202', 'name' => 'KUALA ABANG', 'district_id' => '1102'],
    ['id' => '110201', 'name' => 'BESUL', 'district_id' => '1102'],
    ['id' => '110203', 'name' => 'JENGAI', 'district_id' => '1102'],
    ['id' => '110204', 'name' => 'JERANGAU', 'district_id' => '1102'],
    ['id' => '110205', 'name' => 'KUALA DUNGUN', 'district_id' => '1102'],
    ['id' => '110206', 'name' => 'KUALA PAKA', 'district_id' => '1102'],
    ['id' => '110207', 'name' => 'KUMPAL', 'district_id' => '1102'],
    ['id' => '110208', 'name' => 'PASIR RAJA', 'district_id' => '1102'],
    ['id' => '110209', 'name' => 'RASAU', 'district_id' => '1102'],
    ['id' => '110210', 'name' => 'SURA', 'district_id' => '1102'],
    ['id' => '110211', 'name' => 'HULU PAKA', 'district_id' => '1102'],
    /*
     * === KEMAMAN
     */
    ['id' => '110302', 'name' => 'BANGGUL', 'district_id' => '1103'],
    ['id' => '110301', 'name' => 'BINJAI', 'district_id' => '1103'],
    ['id' => '110303', 'name' => 'BANDI', 'district_id' => '1103'],
    ['id' => '110304', 'name' => 'CUKAI', 'district_id' => '1103'],
    ['id' => '110305', 'name' => 'KEMASIK', 'district_id' => '1103'],
    ['id' => '110306', 'name' => 'KERTEH', 'district_id' => '1103'],
    ['id' => '110307', 'name' => 'KIJAL', 'district_id' => '1103'],
    ['id' => '110308', 'name' => 'PASIR SEMUT', 'district_id' => '1103'],
    ['id' => '110309', 'name' => 'TEBAK', 'district_id' => '1103'],
    ['id' => '110310', 'name' => 'TELUK KALUNG', 'district_id' => '1103'],
    ['id' => '110311', 'name' => 'HULU CUKAI', 'district_id' => '1103'],
    ['id' => '110312', 'name' => 'HULU JABUR', 'district_id' => '1103'],
    /*
     * === KUALA TERENGGANU
     */
    ['id' => '110401', 'name' => 'ATAS TOL', 'district_id' => '1104'],
    ['id' => '110402', 'name' => 'BATU BURUK', 'district_id' => '1104'],
    ['id' => '110403', 'name' => 'BATU RAKIT', 'district_id' => '1104'],
    ['id' => '110404', 'name' => 'BELARA', 'district_id' => '1104'],
    ['id' => '110405', 'name' => 'BUKIT BESAR', 'district_id' => '1104'],
    ['id' => '110406', 'name' => 'CABANG TIGA', 'district_id' => '1104'],
    ['id' => '110407', 'name' => 'CENERIANG', 'district_id' => '1104'],
    ['id' => '110408', 'name' => 'GELUGUR KEDAI', 'district_id' => '1104'],
    ['id' => '110409', 'name' => 'GELUGUR RAJA', 'district_id' => '1104'],
    ['id' => '110410', 'name' => 'KUALA IBAI', 'district_id' => '1104'],
    ['id' => '110411', 'name' => 'KUALA NERUS', 'district_id' => '1104'],
    ['id' => '110412', 'name' => 'KUBANG PARIT', 'district_id' => '1104'],
    ['id' => '110413', 'name' => 'KEPUNG', 'district_id' => '1104'],
    ['id' => '110414', 'name' => 'LOSONG', 'district_id' => '1104'],
    ['id' => '110415', 'name' => 'MANIR', 'district_id' => '1104'],
    ['id' => '110416', 'name' => 'PALUH', 'district_id' => '1104'],
    ['id' => '110417', 'name' => 'PENGADANG BULUH', 'district_id' => '1104'],
    ['id' => '110418', 'name' => 'PULAU-PULAU', 'district_id' => '1104'],
    ['id' => '110419', 'name' => 'PULAU REDANG', 'district_id' => '1104'],
    ['id' => '110420', 'name' => 'RENGAS', 'district_id' => '1104'],
    ['id' => '110421', 'name' => 'SERADA', 'district_id' => '1104'],
    ['id' => '110422', 'name' => 'TOK JAMAL', 'district_id' => '1104'],
    ['id' => '110423', 'name' => 'BANDAR KUALA TERENGGANU', 'district_id' => '1104'],
    /*
     * === MARANG
     */
    ['id' => '110501', 'name' => 'JERUNG', 'district_id' => '1105'],
    ['id' => '110502', 'name' => 'MERCANG', 'district_id' => '1105'],
    ['id' => '110503', 'name' => 'PULAU KERENGGA', 'district_id' => '1105'],
    ['id' => '110504', 'name' => 'RUSILA', 'district_id' => '1105'],
    ['id' => '110505', 'name' => 'BUKIT PAYUNG', 'district_id' => '1105'],
    ['id' => '110506', 'name' => 'ALUR LIMBAT', 'district_id' => '1105'],
    /*
     * === HULU TERENGGANU
     */
    ['id' => '110601', 'name' => 'JENAGUR', 'district_id' => '1106'],
    ['id' => '110602', 'name' => 'KUALA BERANG', 'district_id' => '1106'],
    ['id' => '110603', 'name' => 'KUALA TELEMUNG', 'district_id' => '1106'],
    ['id' => '110604', 'name' => 'PENGHULU DIMAN', 'district_id' => '1106'],
    ['id' => '110605', 'name' => 'TANGGUL', 'district_id' => '1106'],
    ['id' => '110606', 'name' => 'TERSAT', 'district_id' => '1106'],
    ['id' => '110607', 'name' => 'HULU BERANG', 'district_id' => '1106'],
    ['id' => '110608', 'name' => 'HULU TELEMUNG', 'district_id' => '1106'],
    ['id' => '110609', 'name' => 'HULU TERENGGANU', 'district_id' => '1106'],
    /*
     * === SETIU
     */
    ['id' => '110701', 'name' => 'HULU SETIU', 'district_id' => '1107'],
    ['id' => '110702', 'name' => 'PANTAI', 'district_id' => '1107'],
    ['id' => '110703', 'name' => 'GUNTUNG', 'district_id' => '1107'],
    ['id' => '110704', 'name' => 'TASIK', 'district_id' => '1107'],
    ['id' => '110705', 'name' => 'CALUK', 'district_id' => '1107'],
    ['id' => '110706', 'name' => 'MERANG', 'district_id' => '1107'],
    ['id' => '110707', 'name' => 'HULU NERUS', 'district_id' => '1107'],

    /*
     * pulau pinang
     */
    /*
     * === SEBERANG PRAI TENGAH
     */
    ['id' => '070101', 'name' => 'MUKIM 1', 'district_id' => '0701'],
    ['id' => '070102', 'name' => 'BANDAR PRAI (MUKIM 1A)', 'district_id' => '0701'],
    ['id' => '070103', 'name' => 'MUKIM 2', 'district_id' => '0701'],
    ['id' => '070104', 'name' => 'MUKIM 3', 'district_id' => '0701'],
    ['id' => '070105', 'name' => 'MUKIM 4', 'district_id' => '0701'],
    ['id' => '070106', 'name' => 'MUKIM 5', 'district_id' => '0701'],
    ['id' => '070107', 'name' => 'MUKIM 6', 'district_id' => '0701'],
    ['id' => '070108', 'name' => 'MUKIM 7', 'district_id' => '0701'],
    ['id' => '070109', 'name' => 'MUKIM 8', 'district_id' => '0701'],
    ['id' => '070110', 'name' => 'MUKIM 9', 'district_id' => '0701'],
    ['id' => '070111', 'name' => 'MUKIM 10', 'district_id' => '0701'],
    ['id' => '070112', 'name' => 'MUKIM 11', 'district_id' => '0701'],
    ['id' => '070113', 'name' => 'MUKIM 12', 'district_id' => '0701'],
    ['id' => '070114', 'name' => 'MUKIM 13', 'district_id' => '0701'],
    ['id' => '070115', 'name' => 'MUKIM 14', 'district_id' => '0701'],
    ['id' => '070116', 'name' => 'MUKIM 15', 'district_id' => '0701'],
    ['id' => '070117', 'name' => 'MUKIM 16', 'district_id' => '0701'],
    ['id' => '070118', 'name' => 'MUKIM 17', 'district_id' => '0701'],
    ['id' => '070119', 'name' => 'MUKIM 18', 'district_id' => '0701'],
    ['id' => '070120', 'name' => 'MUKIM 19', 'district_id' => '0701'],
    ['id' => '070121', 'name' => 'MUKIM 20', 'district_id' => '0701'],
    ['id' => '070122', 'name' => 'MUKIM 21', 'district_id' => '0701'],
    /*
     * === SEBERANG PRAI UTARA
     */
    ['id' => '070201', 'name' => 'MUKIM 1', 'district_id' => '0702'],
    ['id' => '070202', 'name' => 'MUKIM 2', 'district_id' => '0702'],
    ['id' => '070203', 'name' => 'MUKIM 3', 'district_id' => '0702'],
    ['id' => '070204', 'name' => 'MUKIM 4', 'district_id' => '0702'],
    ['id' => '070205', 'name' => 'MUKIM 5', 'district_id' => '0702'],
    ['id' => '070206', 'name' => 'MUKIM 6', 'district_id' => '0702'],
    ['id' => '070207', 'name' => 'MUKIM 7', 'district_id' => '0702'],
    ['id' => '070208', 'name' => 'MUKIM 8', 'district_id' => '0702'],
    ['id' => '070209', 'name' => 'MUKIM 9', 'district_id' => '0702'],
    ['id' => '070210', 'name' => 'MUKIM 10', 'district_id' => '0702'],
    ['id' => '070211', 'name' => 'MUKIM 11', 'district_id' => '0702'],
    ['id' => '070212', 'name' => 'MUKIM 12', 'district_id' => '0702'],
    ['id' => '070213', 'name' => 'MUKIM 13', 'district_id' => '0702'],
    ['id' => '070214', 'name' => 'MUKIM 14', 'district_id' => '0702'],
    ['id' => '070215', 'name' => 'MUKIM 15', 'district_id' => '0702'],
    ['id' => '070216', 'name' => 'MUKIM 16', 'district_id' => '0702'],
    /*
     * === SEBERANG PRAI SELATAN
     */
    ['id' => '070301', 'name' => 'MUKIM 1', 'district_id' => '0703'],
    ['id' => '070302', 'name' => 'MUKIM 2', 'district_id' => '0703'],
    ['id' => '070303', 'name' => 'MUKIM 3', 'district_id' => '0703'],
    ['id' => '070304', 'name' => 'MUKIM 4', 'district_id' => '0703'],
    ['id' => '070305', 'name' => 'MUKIM 5', 'district_id' => '0703'],
    ['id' => '070306', 'name' => 'MUKIM 6', 'district_id' => '0703'],
    ['id' => '070307', 'name' => 'MUKIM 7', 'district_id' => '0703'],
    ['id' => '070308', 'name' => 'MUKIM 8', 'district_id' => '0703'],
    ['id' => '070309', 'name' => 'MUKIM 9', 'district_id' => '0703'],
    ['id' => '070310', 'name' => 'MUKIM 10', 'district_id' => '0703'],
    ['id' => '070311', 'name' => 'MUKIM 11', 'district_id' => '0703'],
    ['id' => '070312', 'name' => 'MUKIM 12', 'district_id' => '0703'],
    ['id' => '070313', 'name' => 'MUKIM 13', 'district_id' => '0703'],
    ['id' => '070314', 'name' => 'MUKIM 14', 'district_id' => '0703'],
    ['id' => '070315', 'name' => 'MUKIM 15', 'district_id' => '0703'],
    ['id' => '070316', 'name' => 'MUKIM 16', 'district_id' => '0703'],
    /*
     * === TIMUR LAUT
     */
    ['id' => '070401', 'name' => 'MUKIM 13 (PAYA TERUBONG)', 'district_id' => '0704'],
    ['id' => '070402', 'name' => 'MUKIM 14 (BUKIT PAYA TERUBONG)', 'district_id' => '0704'],
    ['id' => '070403', 'name' => 'MUKIM 15 (BUKIT AYER ITAM)', 'district_id' => '0704'],
    ['id' => '070404', 'name' => 'MUKIM 16 (AYER ITAM)', 'district_id' => '0704'],
    ['id' => '070405', 'name' => 'MUKIM 17 (BATU FERINGGI)', 'district_id' => '0704'],
    ['id' => '070406', 'name' => 'MUKIM 18 (TANJONG TOKONG)', 'district_id' => '0704'],
    ['id' => '070407', 'name' => 'BANDARAYA GEORGE TOWN', 'district_id' => '0704'],
    /*
     * === BARAT DAYA
     */
    ['id' => '070501', 'name' => 'MUKIM A (SUNGAI PINANG)', 'district_id' => '0705'],
    ['id' => '070502', 'name' => 'MUKIM B (SUNGAI RUSA)', 'district_id' => '0705'],
    ['id' => '070503', 'name' => 'MUKIM C (PERMATANG PASIR)', 'district_id' => '0705'],
    ['id' => '070504', 'name' => 'MUKIM D (BAGAN AYER ITAM)', 'district_id' => '0705'],
    ['id' => '070505', 'name' => 'MUKIM E (TITI KERAS)', 'district_id' => '0705'],
    ['id' => '070506', 'name' => 'MUKIM F (KONGSI)', 'district_id' => '0705'],
    ['id' => '070507', 'name' => 'MUKIM G (KAMPONG PAYA)', 'district_id' => '0705'],
    ['id' => '070508', 'name' => 'MUKIM H (SUNGAI BURONG)', 'district_id' => '0705'],
    ['id' => '070509', 'name' => 'MUKIM I (PULAU BETONG)', 'district_id' => '0705'],
    ['id' => '070510', 'name' => 'MUKIM J (DARATAN GINTING)', 'district_id' => '0705'],
    ['id' => '070511', 'name' => 'MUKIM 1 (PANTAI ACHEH)', 'district_id' => '0705'],
    ['id' => '070512', 'name' => 'MUKIM 2 (TEONG BAHANG)', 'district_id' => '0705'],
    ['id' => '070513', 'name' => 'MUKIM 3 (SUNGAI RUSA & BUKIT SUNGAI PINANG)', 'district_id' => '0705'], // serius shit? clash with mukim A, mukim B
    ['id' => '070514', 'name' => 'MUKIM 4 (BATU ITAM)', 'district_id' => '0705'],
    ['id' => '070515', 'name' => 'MUKIM 5 (BUKIT BALIK PULAU)', 'district_id' => '0705'],
    ['id' => '070516', 'name' => 'MUKIM 6 (PONDOK UPEH)', 'district_id' => '0705'],
    ['id' => '070517', 'name' => 'MUKIM 7 (BUKIT GINTING)', 'district_id' => '0705'],
    ['id' => '070518', 'name' => 'MUKIM 8 (BUKIT PASIR PANJANG)', 'district_id' => '0705'],
    ['id' => '070519', 'name' => 'MUKIM 9 (BUKIT GEMUROH)', 'district_id' => '0705'],
    ['id' => '070520', 'name' => 'MUKIM 10 (BUKIT RELAU)', 'district_id' => '0705'],
    ['id' => '070521', 'name' => 'MUKIM 11 (TELOK KUMBAR)', 'district_id' => '0705'],
    ['id' => '070522', 'name' => 'MUKIM 12 (BAYAN LEPAS)', 'district_id' => '0705'],
    /*
     * perak
     */
    /*
     * === BATANG PADANG
     */
    ['id' => '080101', 'name' => 'BATANG PADANG', 'district_id' => '0801'],
    ['id' => '080102', 'name' => 'BIDOR', 'district_id' => '0801'],
    ['id' => '080103', 'name' => 'CHENDERIANG', 'district_id' => '0801'],
    ['id' => '080104', 'name' => 'SLIM', 'district_id' => '0801'],
    ['id' => '080105', 'name' => 'SUNGKAI', 'district_id' => '0801'],
    ['id' => '080106', 'name' => 'ULU BERNAM TIMOR & BARAT', 'district_id' => '0801'],
    /*
     * === MANJUNG (DINDING)
     */
    ['id' => '080201', 'name' => 'BERUAS', 'district_id' => '0802'],
    ['id' => '080202', 'name' => 'LEKIR', 'district_id' => '0802'],
    ['id' => '080203', 'name' => 'LUMUT', 'district_id' => '0802'],
    ['id' => '080204', 'name' => 'PENGKALAN BAHARU', 'district_id' => '0802'],
    ['id' => '080205', 'name' => 'SITIAWAN', 'district_id' => '0802'],
    /*
     * === KINTA
     */
    ['id' => '080301', 'name' => 'BELANJA', 'district_id' => '0803'],
    ['id' => '080302', 'name' => 'KAMPAR', 'district_id' => '0803'],
    ['id' => '080303', 'name' => 'SUNGAI RAIA', 'district_id' => '0803'],
    ['id' => '080304', 'name' => 'SUNGAI TERAP', 'district_id' => '0803'],
    ['id' => '080305', 'name' => 'TANJONG TUALANG', 'district_id' => '0803'],
    ['id' => '080306', 'name' => 'TEJA', 'district_id' => '0803'],
    ['id' => '080307', 'name' => 'ULU KINTA', 'district_id' => '0803'],
    /*
     * === KERIAN
     */
    ['id' => '080401', 'name' => 'BAGAN SERAI', 'district_id' => '0804'],
    ['id' => '080402', 'name' => 'BAGAN TIANG', 'district_id' => '0804'],
    ['id' => '080403', 'name' => 'BERIAH', 'district_id' => '0804'],
    ['id' => '080404', 'name' => 'GUNONG SEMANGGOL', 'district_id' => '0804'],
    ['id' => '080405', 'name' => 'KUALA KURAU', 'district_id' => '0804'],
    ['id' => '080406', 'name' => 'PARIT BUNTAR', 'district_id' => '0804'],
    ['id' => '080407', 'name' => 'SELINSING', 'district_id' => '0804'],
    ['id' => '080408', 'name' => 'TANJONG PIANDANG', 'district_id' => '0804'],
    /*
     * === KUALA KANGSAR
     */
    ['id' => '080501', 'name' => 'CHENGAR GALAH', 'district_id' => '0805'],
    ['id' => '080502', 'name' => 'KAMPONG BUAYA', 'district_id' => '0805'],
    ['id' => '080503', 'name' => 'KOTA LAMA KANAN', 'district_id' => '0805'],
    ['id' => '080504', 'name' => 'KOTA LAMA KIRI', 'district_id' => '0805'],
    ['id' => '080505', 'name' => 'LUBOK MERBAU', 'district_id' => '0805'],
    ['id' => '080506', 'name' => 'PULAU KAMIRI', 'district_id' => '0805'],
    ['id' => '080507', 'name' => 'SAIONG', 'district_id' => '0805'],
    ['id' => '080508', 'name' => 'SENGGANG', 'district_id' => '0805'],
    ['id' => '080509', 'name' => 'SUNGAI SIPUT', 'district_id' => '0805'],
    /*
     * === LARUT DAN MATANG
     */
    ['id' => '080601', 'name' => 'ASAM KUMBANG', 'district_id' => '0806'],
    ['id' => '080602', 'name' => 'BATU KURAU', 'district_id' => '0806'],
    ['id' => '080603', 'name' => 'BUKIT GANTANG', 'district_id' => '0806'],
    ['id' => '080604', 'name' => 'ULU IJOK', 'district_id' => '0806'],
    ['id' => '080605', 'name' => 'JEBONG', 'district_id' => '0806'],
    ['id' => '080606', 'name' => 'KAMUNTING', 'district_id' => '0806'],
    ['id' => '080607', 'name' => 'PENGKALAN AOR', 'district_id' => '0806'],
    ['id' => '080608', 'name' => 'SELAMA', 'district_id' => '0806'],
    ['id' => '080609', 'name' => 'SIMPANG', 'district_id' => '0806'],
    ['id' => '080610', 'name' => 'SUNGAI LIMAU', 'district_id' => '0806'],
    ['id' => '080611', 'name' => 'SUNGAI TINGGI', 'district_id' => '0806'],
    ['id' => '080612', 'name' => 'TERONG', 'district_id' => '0806'],
    ['id' => '080613', 'name' => 'TUPAI', 'district_id' => '0806'],
    ['id' => '080614', 'name' => 'ULU SELAMA', 'district_id' => '0806'],
    /*
     * === HILIR PERAK
     */
    ['id' => '080701', 'name' => 'BAGAN DATOH', 'district_id' => '0807'],
    ['id' => '080702', 'name' => 'CHANGKAT JONG', 'district_id' => '0807'],
    ['id' => '080703', 'name' => 'DURIAN SEBATANG', 'district_id' => '0807'],
    ['id' => '080704', 'name' => 'HUTAN MELINTANG', 'district_id' => '0807'],
    ['id' => '080705', 'name' => 'LABU KUBONG', 'district_id' => '0807'],
    ['id' => '080706', 'name' => 'RUNGKUP', 'district_id' => '0807'],
    ['id' => '080707', 'name' => 'SUNGAI DURIAN', 'district_id' => '0807'],
    ['id' => '080708', 'name' => 'SUNGAI MANIK', 'district_id' => '0807'],
    ['id' => '080709', 'name' => 'TELOK BAHARU', 'district_id' => '0807'],
    /*
     * === ULU PERAK
     */
    ['id' => '080801', 'name' => 'BELUKAR SEMANG', 'district_id' => '0808'],
    ['id' => '080802', 'name' => 'BELUM', 'district_id' => '0808'],
    ['id' => '080803', 'name' => 'DURIAN PIPIT', 'district_id' => '0808'],
    ['id' => '080804', 'name' => 'GRIK', 'district_id' => '0808'],
    ['id' => '080805', 'name' => 'KENERING', 'district_id' => '0808'],
    ['id' => '080806', 'name' => 'KERUNAI', 'district_id' => '0808'],
    ['id' => '080807', 'name' => 'PENGKALAN HULU', 'district_id' => '0808'],
    ['id' => '080808', 'name' => 'LENGGONG', 'district_id' => '0808'],
    ['id' => '080809', 'name' => 'TEMELONG', 'district_id' => '0808'],
    ['id' => '080810', 'name' => 'TEMENGOR', 'district_id' => '0808'],
    /*
     * === PERAK TENGAH
     */
    ['id' => '080901', 'name' => 'BANDAR', 'district_id' => '0809'],
    ['id' => '080902', 'name' => 'BELANJA', 'district_id' => '0809'], //CLASH WITH KINTA.BELANJA ?
    ['id' => '080903', 'name' => 'BOTA', 'district_id' => '0809'],
    ['id' => '080904', 'name' => 'JAYA BAHARU', 'district_id' => '0809'],
    ['id' => '080905', 'name' => 'KAMPONG GAJAH', 'district_id' => '0809'],
    ['id' => '080906', 'name' => 'KOTA SETIA', 'district_id' => '0809'],
    ['id' => '080907', 'name' => 'LAMBOR KANAN', 'district_id' => '0809'],
    ['id' => '080908', 'name' => 'LAMBOR KIRI', 'district_id' => '0809'],
    ['id' => '080909', 'name' => 'LAYANG-LAYANG', 'district_id' => '0809'],
    ['id' => '080910', 'name' => 'PASIR PANJANG ULU', 'district_id' => '0809'],
    ['id' => '080911', 'name' => 'PASIR SALAK', 'district_id' => '0809'],
    ['id' => '080912', 'name' => 'PULAU TIGA', 'district_id' => '0809'],

    /*
     * pahang
     */
    /*
     * === BENTONG
     */
    ['id' => '060101', 'name' => 'BENTONG', 'district_id' => '0601'],
    ['id' => '060102', 'name' => 'PELANGAI', 'district_id' => '0601'],
    ['id' => '060103', 'name' => 'SABAI', 'district_id' => '0601'],
    /*
     * === CAMERON HIGHLANDS
     */
    ['id' => '060201', 'name' => 'RINGLET', 'district_id' => '0602'],
    ['id' => '060202', 'name' => 'TANAH RATA', 'district_id' => '0602'],
    ['id' => '060203', 'name' => 'ULU TELOM', 'district_id' => '0602'],
    /*
     * === JERANTUT
     */
    ['id' => '060301', 'name' => 'BURAU', 'district_id' => '0603'],
    ['id' => '060302', 'name' => 'KELOLA', 'district_id' => '0603'],
    ['id' => '060303', 'name' => 'KUALA TEMBELING', 'district_id' => '0603'],
    ['id' => '060304', 'name' => 'PEDAH', 'district_id' => '0603'],
    ['id' => '060305', 'name' => 'PULAU TAWAR', 'district_id' => '0603'],
    ['id' => '060306', 'name' => 'TEBING TINGGI', 'district_id' => '0603'],
    ['id' => '060307', 'name' => 'TEH', 'district_id' => '0603'],
    ['id' => '060308', 'name' => 'TEMBELING', 'district_id' => '0603'],
    ['id' => '060309', 'name' => 'ULU CHEKA', 'district_id' => '0603'],
    ['id' => '060310', 'name' => 'ULU TEMBELING', 'district_id' => '0603'],
    /*
     * === KUANTAN
     */
    ['id' => '060401', 'name' => 'BESERAH', 'district_id' => '0604'],
    ['id' => '060402', 'name' => 'KUALA KUANTAN', 'district_id' => '0604'],
    ['id' => '060403', 'name' => 'PENOR', 'district_id' => '0604'],
    ['id' => '060404', 'name' => 'SUNGAI KARANG', 'district_id' => '0604'],
    ['id' => '060405', 'name' => 'ULU KUANTAN', 'district_id' => '0604'],
    ['id' => '060406', 'name' => 'ULU LEPAR', 'district_id' => '0604'],
    /*
     * === LIPIS
     */
    ['id' => '060501', 'name' => 'BATU YON', 'district_id' => '0605'],
    ['id' => '060502', 'name' => 'BUDU', 'district_id' => '0605'],
    ['id' => '060503', 'name' => 'CHEKA', 'district_id' => '0605'],
    ['id' => '060504', 'name' => 'GUA', 'district_id' => '0605'],
    ['id' => '060505', 'name' => 'KECHAU', 'district_id' => '0605'],
    ['id' => '060506', 'name' => 'KUALA LIPIS', 'district_id' => '0605'],
    ['id' => '060507', 'name' => 'PENJOM', 'district_id' => '0605'],
    ['id' => '060508', 'name' => 'TANJUNG BESAR', 'district_id' => '0605'],
    ['id' => '060509', 'name' => 'TELANG', 'district_id' => '0605'],
    ['id' => '060510', 'name' => 'ULU JELAI', 'district_id' => '0605'],
    /*
     * === PEKAN
     */
    ['id' => '060601', 'name' => 'BEBAR', 'district_id' => '0606'],
    ['id' => '060602', 'name' => 'GANCHONG', 'district_id' => '0606'],
    ['id' => '060603', 'name' => 'KUALA PAHANG', 'district_id' => '0606'],
    ['id' => '060604', 'name' => 'LANGGAR', 'district_id' => '0606'],
    ['id' => '060605', 'name' => 'LEPAR', 'district_id' => '0606'],
    ['id' => '060606', 'name' => 'PAHANG TUA', 'district_id' => '0606'],
    ['id' => '060607', 'name' => 'PEKAN', 'district_id' => '0606'],
    ['id' => '060608', 'name' => 'PENYOR', 'district_id' => '0606'],
    ['id' => '060609', 'name' => 'PULAU MANIS', 'district_id' => '0606'],
    ['id' => '060610', 'name' => 'PULAU RUSA', 'district_id' => '0606'],
    ['id' => '060611', 'name' => 'TEMAI', 'district_id' => '0606'],
    /*
     * === RAUB
     */
    ['id' => '060701', 'name' => 'BATU TALAM', 'district_id' => '0607'],
    ['id' => '060702', 'name' => 'DONG', 'district_id' => '0607'],
    ['id' => '060703', 'name' => 'GALI', 'district_id' => '0607'],
    ['id' => '060704', 'name' => 'SEGA', 'district_id' => '0607'],
    ['id' => '060705', 'name' => 'SEMANTAN ULU', 'district_id' => '0607'],
    ['id' => '060706', 'name' => 'TERAS', 'district_id' => '0607'],
    ['id' => '060707', 'name' => 'ULU DONG', 'district_id' => '0607'],
    /*
     * === TEMERLOH
     */
    ['id' => '060801', 'name' => 'BANGAU', 'district_id' => '0608'],
    // ['id' => '060802', 'name' => '??', 'district_id' => '0608'],
    ['id' => '060803', 'name' => 'JENDERAK', 'district_id' => '0608'],
    ['id' => '060804', 'name' => 'KERDAU', 'district_id' => '0608'],
    ['id' => '060805', 'name' => 'LEBAK', 'district_id' => '0608'],
    ['id' => '060806', 'name' => 'LIPAT KIJANG', 'district_id' => '0608'],
    ['id' => '060807', 'name' => 'MENTAKAB', 'district_id' => '0608'],
    ['id' => '060808', 'name' => 'PERAK', 'district_id' => '0608'],
    ['id' => '060809', 'name' => 'SANGGANG', 'district_id' => '0608'],
    ['id' => '060810', 'name' => 'SEMANTAN', 'district_id' => '0608'],
    ['id' => '060811', 'name' => 'SONGSANG', 'district_id' => '0608'],
    /*
     * === ROMPIN
     */
    ['id' => '060901', 'name' => 'ENDAU', 'district_id' => '0609'],
    ['id' => '060902', 'name' => 'KERATONG', 'district_id' => '0609'],
    ['id' => '060903', 'name' => 'PONTIAN', 'district_id' => '0609'],
    ['id' => '060904', 'name' => 'ROMPIN', 'district_id' => '0609'],
    ['id' => '060905', 'name' => 'TIOMAN', 'district_id' => '0609'],
    /*
     * === MARAN
     */
    ['id' => '061001', 'name' => 'BUKIT SEGUMPAL', 'district_id' => '0610'],
    ['id' => '061002', 'name' => 'CHENOR', 'district_id' => '0610'],
    ['id' => '061003', 'name' => 'KERTAU', 'district_id' => '0610'],
    ['id' => '061004', 'name' => 'LUIT', 'district_id' => '0610'],
    /*
     * === BERA
     */
    ['id' => '061101', 'name' => 'BERA', 'district_id' => '0611'],
    ['id' => '061102', 'name' => 'TRIANG', 'district_id' => '0611'],

    /*
     * selangor
     */
    /*
     * === KLANG
     */
    ['id' => '100101', 'name' => 'KAPAR', 'district_id' => '1001'],
    ['id' => '100102', 'name' => 'KLANG', 'district_id' => '1001'],
    // ['id' => '100103', 'name' => 'BANDAR KLANG', 'district_id' => '1001'], // mv 100140

    ['id' => '100140', 'name' => 'BANDAR KLANG', 'district_id' => '1001'],
    ['id' => '100141', 'name' => 'BANDAR PORT SWETTENHAM', 'district_id' => '1001'],
    ['id' => '100142', 'name' => 'BANDAR SULTAN SULAIMAN', 'district_id' => '1001'],
    ['id' => '100143', 'name' => 'BANDAR SHAH ALAM', 'district_id' => '1001'],

    ['id' => '100170', 'name' => 'PEKAN BUKIT KEMUNING', 'district_id' => '1001'],
    ['id' => '100171', 'name' => 'PEKAN KAPAR', 'district_id' => '1001'],
    ['id' => '100172', 'name' => 'PEKAN MERU', 'district_id' => '1001'],
    ['id' => '100173', 'name' => 'PEKAN TELOK MENEGUN', 'district_id' => '1001'],
    ['id' => '100174', 'name' => 'PEKAN BATU EMPAT', 'district_id' => '1001'],
    ['id' => '100175', 'name' => 'PEKAN PANDAMARAN', 'district_id' => '1001'],
    /*
     * === KUALA LANGAT
     */
    ['id' => '100201', 'name' => 'BANDAR', 'district_id' => '1002'],
    ['id' => '100202', 'name' => 'BATU', 'district_id' => '1002'],
    ['id' => '100203', 'name' => 'JUGRA', 'district_id' => '1002'],
    ['id' => '100204', 'name' => 'KELANANG', 'district_id' => '1002'],
    ['id' => '100205', 'name' => 'MORIB', 'district_id' => '1002'],
    ['id' => '100206', 'name' => 'TANJONG DUABELAS', 'district_id' => '1002'],
    ['id' => '100207', 'name' => 'TELOK PANGLIMA GARANG', 'district_id' => '1002'],

    ['id' => '100240', 'name' => 'BANDAR BANTING', 'district_id' => '1002'],
    ['id' => '100241', 'name' => 'BANDAR JENJAROM', 'district_id' => '1002'],
    ['id' => '100242', 'name' => 'BANDAR SIJANGKANG', 'district_id' => '1002'],
    ['id' => '100243', 'name' => 'BANDAR TANJONG SEPAT', 'district_id' => '1002'],
    ['id' => '100244', 'name' => 'BANDAR TELOK PANGLIMA GARANG', 'district_id' => '1002'],

    ['id' => '100270', 'name' => 'PEKAN BATU', 'district_id' => '1002'],
    ['id' => '100271', 'name' => 'PEKAN BUKIT CHANGGANG', 'district_id' => '1002'],
    ['id' => '100272', 'name' => 'PEKAN CHODOI', 'district_id' => '1002'],
    ['id' => '100273', 'name' => 'PEKAN JENJAROM', 'district_id' => '1002'],
    ['id' => '100274', 'name' => 'PEKAN KANCHONG', 'district_id' => '1002'],
    ['id' => '100275', 'name' => 'PEKAN KANCHONG DARAT', 'district_id' => '1002'],
    ['id' => '100276', 'name' => 'PEKAN KELANANG BATU ENAM', 'district_id' => '1002'],
    ['id' => '100277', 'name' => 'PEKAN MORIB', 'district_id' => '1002'],
    ['id' => '100278', 'name' => 'PEKAN SIJANGKANG', 'district_id' => '1002'],
    ['id' => '100279', 'name' => 'PEKAN PERMATANG PASIR', 'district_id' => '1002'],
    ['id' => '100280', 'name' => 'PEKAN SIMPANG MORIB', 'district_id' => '1002'],
    ['id' => '100281', 'name' => 'PEKAN SUNGAI MANGGIS', 'district_id' => '1002'],
    ['id' => '100282', 'name' => 'PEKAN SUNGAI RABA', 'district_id' => '1002'],
    ['id' => '100283', 'name' => 'PEKAN TANJONG DUABELAS', 'district_id' => '1002'],
    ['id' => '100284', 'name' => 'PEKAN TELOK DATOK', 'district_id' => '1002'],
    ['id' => '100286', 'name' => 'PEKAN TONGKAH', 'district_id' => '1002'],
    ['id' => '100287', 'name' => 'PEKAN TELOK', 'district_id' => '1002'],
    /*
     * === 1003 ??? // deprecated?
     */
    /*
     * === KUALA SELANGOR
     */
    ['id' => '100401', 'name' => 'API-API', 'district_id' => '1004'],
    // ['id' => '100402', 'name' => 'BATANG BERJUNTAI', 'district_id' => '1004'], // deprecated?
    ['id' => '100405', 'name' => 'IJOK', 'district_id' => '1004'],
    ['id' => '100406', 'name' => 'JERAM', 'district_id' => '1004'],
    ['id' => '100407', 'name' => 'KUALA SELANGOR', 'district_id' => '1004'],
    ['id' => '100408', 'name' => 'PASANGAN', 'district_id' => '1004'],
    ['id' => '100409', 'name' => 'TANJONG KARANG', 'district_id' => '1004'],
    // ['id' => '100408', 'name' => 'UJONG PERMATANG', 'district_id' => '1004'], // deprecated?
    // ['id' => '100409', 'name' => 'ULU TINGGI', 'district_id' => '1004'], // deprecated?
    ['id' => '100410', 'name' => 'BESTARI JAYA', 'district_id' => '1004'],

    ['id' => '100440', 'name' => 'BANDAR KUALA SELANGOR', 'district_id' => '1004'],
    ['id' => '100441', 'name' => 'BANDAR TANJONG KARANG', 'district_id' => '1004'],

    ['id' => '100470', 'name' => 'PEKAN ASAM JAWA', 'district_id' => '1004'],
    // ['id' => '100471', 'name' => '??', 'district_id' => '1004'], // deprecated?
    ['id' => '100472', 'name' => 'PEKAN BUKIT ROTAN', 'district_id' => '1004'],
    ['id' => '100473', 'name' => 'PEKAN JERAM', 'district_id' => '1004'],
    ['id' => '100474', 'name' => 'PEKAN KAMPONG KUANTAN', 'district_id' => '1004'],
    ['id' => '100475', 'name' => 'PEKAN KUALA SUNGAI BULOH', 'district_id' => '1004'],
    ['id' => '100476', 'name' => 'PEKAN PASIR PENAMPANG', 'district_id' => '1004'],
    ['id' => '100477', 'name' => 'PEKAN SIMPANG TIGA', 'district_id' => '1004'],
    ['id' => '100478', 'name' => 'PEKAN TANJONG KARANG', 'district_id' => '1004'],
    ['id' => '100479', 'name' => 'PEKAN BUKTI BELIMBING', 'district_id' => '1004'],
    ['id' => '100480', 'name' => 'PEKAN BUKIT TALANG', 'district_id' => '1004'],
    ['id' => '100481', 'name' => 'PEKAN KAMBUNG BARU HULU TIRAM BURUK', 'district_id' => '1004'],
    ['id' => '100482', 'name' => 'PEKAN PARTI MAHANG', 'district_id' => '1004'],
    ['id' => '100483', 'name' => 'PEKAN SIMPANG TIGA IJOK', 'district_id' => '1004'],
    ['id' => '100484', 'name' => 'PEKAN SUNGAI SEMBILANG', 'district_id' => '1004'],
    ['id' => '100485', 'name' => 'PEKAN TAMAN PKNS', 'district_id' => '1004'],
    ['id' => '100488', 'name' => 'PEKAN TAMBAK JAWA', 'district_id' => '1004'],
    // ['id' => '100489', 'name' => '??', 'district_id' => '1004'], // deprecated?
    // ['id' => '100490', 'name' => '??', 'district_id' => '1004'], // deprecated?
    ['id' => '100491', 'name' => 'PEKAN BESTARI JAYA', 'district_id' => '1004'],
    /*
     * === SABAK BERNAM
     */
    ['id' => '100501', 'name' => 'BAGAN NAKHODA OMAR', 'district_id' => '1005'],
    ['id' => '100502', 'name' => 'PANCANG BEDENA', 'district_id' => '1005'],
    ['id' => '100503', 'name' => 'PASIR PANJANG', 'district_id' => '1005'],
    ['id' => '100504', 'name' => 'SABAK', 'district_id' => '1005'],
    ['id' => '100505', 'name' => 'SUNGAI PANJANG', 'district_id' => '1005'],

    ['id' => '100570', 'name' => 'PEKAN BAGAN TERAP', 'district_id' => '1005'],
    ['id' => '100571', 'name' => 'PEKAN PARIT ENAM', 'district_id' => '1005'],
    ['id' => '100572', 'name' => 'PEKAN PARIT SEMBILAN', 'district_id' => '1005'],
    ['id' => '100573', 'name' => 'PEKAN SEKINCHAN', 'district_id' => '1005'],
    // ['id' => '100574', 'name' => '??', 'district_id' => '1005'], // deprecated?
    ['id' => '100575', 'name' => 'PEKAN SABAK', 'district_id' => '1005'],
    ['id' => '100576', 'name' => 'PEKAN SUNGAI AIR TAWAR', 'district_id' => '1005'],
    ['id' => '100577', 'name' => 'PEKAN SUNGAI SEPINTAS', 'district_id' => '1005'],
    ['id' => '100578', 'name' => 'PEKAN BAGAN NAKHODA OMAR', 'district_id' => '1005'],
    ['id' => '100579', 'name' => 'PEKAN PARIT BARU', 'district_id' => '1005'],
    ['id' => '100580', 'name' => 'PEKAN PASIR PANJANG', 'district_id' => '1005'],
    ['id' => '100581', 'name' => 'PEKAN SEKINCHAN SITE A', 'district_id' => '1005'],
    ['id' => '100582', 'name' => 'PEKAN SUNGAI BESAR', 'district_id' => '1005'],
    ['id' => '100583', 'name' => 'PEKAN SUNGAI HAJI DORANI', 'district_id' => '1005'],
    ['id' => '100584', 'name' => 'PEKAN SUNGAI NIBONG', 'district_id' => '1005'],
    /*
     * === ULU LANGAT
     */
    ['id' => '100601', 'name' => 'BERANANG', 'district_id' => '1006'],
    ['id' => '100602', 'name' => 'CHERAS', 'district_id' => '1006'],
    ['id' => '100603', 'name' => 'AMPANG', 'district_id' => '1006'],
    ['id' => '100604', 'name' => 'HULU LANGAT', 'district_id' => '1006'],
    ['id' => '100605', 'name' => 'HULU SEMENYIH', 'district_id' => '1006'],
    ['id' => '100606', 'name' => 'KAJANG', 'district_id' => '1006'],
    ['id' => '100607', 'name' => 'SEMENYIH', 'district_id' => '1006'],

    ['id' => '100640', 'name' => 'BANDAR CHERAS', 'district_id' => '1006'],
    ['id' => '100641', 'name' => 'BANDAR ULU LANGAT', 'district_id' => '1006'],
    ['id' => '100642', 'name' => 'BANDAR KAJANG', 'district_id' => '1006'],
    ['id' => '100643', 'name' => 'BANDAR SEMENYIH', 'district_id' => '1006'],
    ['id' => '100644', 'name' => 'BANDAR AMPANG', 'district_id' => '1006'],
    ['id' => '100645', 'name' => 'BANDAR BALAKONG', 'district_id' => '1006'],
    ['id' => '100646', 'name' => 'BANDAR BATU 9, CHERAS', 'district_id' => '1006'],
    // ['id' => '100647', 'name' => '??', 'district_id' => '1006'], // deprecated?
    ['id' => '100648', 'name' => 'BANDAR BATU 18, SEMENYIH', 'district_id' => '1006'],
    ['id' => '100649', 'name' => 'BANDAR BATU 26, BERANANG', 'district_id' => '1006'],

    // ['id' => '100670', 'name' => '??', 'district_id' => '1006'], // deprecated?
    ['id' => '100671', 'name' => 'PEKAN BERANANG', 'district_id' => '1006'],
    // ['id' => '100672', 'name' => '??', 'district_id' => '1006'], // deprecated?
    ['id' => '100673', 'name' => 'PEKAN KACAU', 'district_id' => '1006'],
    // ['id' => '100674', 'name' => '??', 'district_id' => '1006'], // deprecated?
    ['id' => '100675', 'name' => 'PEKAN TARUN', 'district_id' => '1006'],
    ['id' => '100676', 'name' => 'PEKAN BANGI LAMA', 'district_id' => '1006'],
    // ['id' => '100677', 'name' => '??', 'district_id' => '1006'], // deprecated?
    ['id' => '100678', 'name' => 'PEKAN BATU 23, SUNGAI LALANG', 'district_id' => '1006'],
    ['id' => '100679', 'name' => 'PEKAN BATU 26, BERANANG', 'district_id' => '1006'],
    ['id' => '100680', 'name' => 'PEKAN BUKIT SUNGAI RAYA', 'district_id' => '1006'],
    ['id' => '100681', 'name' => 'PEKAN CHERAS', 'district_id' => '1006'],
    ['id' => '100682', 'name' => 'PEKAN DESA RAYA', 'district_id' => '1006'],
    ['id' => '100683', 'name' => 'PEKAN DUSUN TUA ULU LANGAT', 'district_id' => '1006'],
    ['id' => '100684', 'name' => 'PEKAN KAJANG', 'district_id' => '1006'],
    ['id' => '100685', 'name' => 'PEKAN KAMPONG PASIR, BATU 14, SEMENYIH', 'district_id' => '1006'],
    ['id' => '100686', 'name' => 'PEKAN KAMPONG SUNGAI TANGKAS', 'district_id' => '1006'],
    ['id' => '100687', 'name' => 'PEKAN RUMAH MURAH SUNGAI LUI', 'district_id' => '1006'],
    ['id' => '100688', 'name' => 'PEKAN SEMENYIH', 'district_id' => '1006'],
    ['id' => '100689', 'name' => 'PEKAN SIMPANG BALAK', 'district_id' => '1006'],
    ['id' => '100690', 'name' => 'PEKAN SRI NANDING', 'district_id' => '1006'],
    ['id' => '100691', 'name' => 'PEKAN SUNGAI KEMBONG BERANANG', 'district_id' => '1006'],
    ['id' => '100692', 'name' => 'PEKAN SUNGAI LUI', 'district_id' => '1006'],
    ['id' => '100693', 'name' => 'PEKAN SUNGAI MAKAU', 'district_id' => '1006'],
    /*
     * === ULU SELANGOR
     */
    ['id' => '100701', 'name' => 'BATANG KALI', 'district_id' => '1007'],
    ['id' => '100702', 'name' => 'BULUH TELOR', 'district_id' => '1007'],
    ['id' => '100703', 'name' => 'AMPANG PECHAH', 'district_id' => '1007'],
    ['id' => '100704', 'name' => 'ULU BERNAM', 'district_id' => '1007'],
    ['id' => '100705', 'name' => 'ULU YAM', 'district_id' => '1007'],
    ['id' => '100706', 'name' => 'KALUMPANG', 'district_id' => '1007'],
    ['id' => '100707', 'name' => 'KERLING', 'district_id' => '1007'],
    ['id' => '100708', 'name' => 'KUALA KALUMPANG', 'district_id' => '1007'],
    ['id' => '100709', 'name' => 'PERETAK', 'district_id' => '1007'],
    ['id' => '100710', 'name' => 'RASA', 'district_id' => '1007'],
    ['id' => '100711', 'name' => 'SERENDAH', 'district_id' => '1007'],
    ['id' => '100712', 'name' => 'SUNGAI GUMUT', 'district_id' => '1007'],
    ['id' => '100713', 'name' => 'SUNGAI TINGGI', 'district_id' => '1007'],

    ['id' => '100740', 'name' => 'BANDAR ULU YAM', 'district_id' => '1007'],
    ['id' => '100741', 'name' => 'BANDAR ULU YAM BAHARU', 'district_id' => '1007'],
    ['id' => '100742', 'name' => 'BANDAR KALUMPANG', 'district_id' => '1007'],
    ['id' => '100743', 'name' => 'BANDAR KUALA KUBU BAHARU', 'district_id' => '1007'],
    ['id' => '100744', 'name' => 'BANDAR RASA', 'district_id' => '1007'],
    ['id' => '100745', 'name' => 'BANDAR SERENDAH', 'district_id' => '1007'],
    ['id' => '100746', 'name' => 'BANDAR BATANG KALI', 'district_id' => '1007'],
    ['id' => '100747', 'name' => 'BANDAR ULU BERNAM I', 'district_id' => '1007'],
    ['id' => '100748', 'name' => 'BANDAR ULU BERNAM II', 'district_id' => '1007'],
    ['id' => '100749', 'name' => 'BANDAR SUNGAI CHIK', 'district_id' => '1007'],

    ['id' => '100770', 'name' => 'PEKAN KERLING', 'district_id' => '1007'],
    ['id' => '100771', 'name' => 'PEKAN PERETAK', 'district_id' => '1007'],
    ['id' => '100772', 'name' => 'PEKAN SIMPANG SUNGAI CHOH', 'district_id' => '1007'],
    /*
     * === PETALING
     */
    ['id' => '100802', 'name' => 'BUKIT RAJA', 'district_id' => '1008'],
    ['id' => '100801', 'name' => 'DAMANSARA', 'district_id' => '1008'],
    ['id' => '100803', 'name' => 'PETALING', 'district_id' => '1008'],
    ['id' => '100804', 'name' => 'SUNGAI BULOH', 'district_id' => '1008'],
    // ['id' => '100805', 'name' => 'BANDAR PETALING JAYA', 'district_id' => '1008'], // mv 100840
    
    ['id' => '100840', 'name' => 'BANDAR PETALING JAYA', 'district_id' => '1008'],
    ['id' => '100841', 'name' => 'BANDAR SHAH ALAM', 'district_id' => '1008'],
    ['id' => '100842', 'name' => 'BANDAR DAMANSARA', 'district_id' => '1008'],
    ['id' => '100843', 'name' => 'BANDAR GLENMARIE', 'district_id' => '1008'],
    ['id' => '100844', 'name' => 'BANDAR PETALING JAYA SELATAN', 'district_id' => '1008'],
    ['id' => '100845', 'name' => 'BANDAR SAUJANA', 'district_id' => '1008'],
    ['id' => '100846', 'name' => 'BANDAR SRI DAMANSARA', 'district_id' => '1008'],
    ['id' => '100847', 'name' => 'BANDAR SUBANG JAYA', 'district_id' => '1008'],
    ['id' => '100848', 'name' => 'BANDAR SUNWAY', 'district_id' => '1008'],

    ['id' => '100870', 'name' => 'PEKAN BATU TIGA', 'district_id' => '1008'],
    ['id' => '100871', 'name' => 'PEKAN MERBAU SEMPAK', 'district_id' => '1008'],
    ['id' => '100872', 'name' => 'PEKAN PUCHONG', 'district_id' => '1008'],
    // ['id' => '100873', 'name' => '??', 'district_id' => '1008'], // deprecated?
    ['id' => '100874', 'name' => 'PEKAN SERDANG', 'district_id' => '1008'],
    ['id' => '100875', 'name' => 'PEKAN SUNGAI BULOH', 'district_id' => '1008'],
    ['id' => '100876', 'name' => 'PEKAN SUNGAI PENCHALA', 'district_id' => '1008'],
    ['id' => '100877', 'name' => 'PEKAN CEMPAKA', 'district_id' => '1008'],
    ['id' => '100878', 'name' => 'PEKAN COUNTRY HEIGHT', 'district_id' => '1008'],
    ['id' => '100879', 'name' => 'PEKAN DESA PUCHONG', 'district_id' => '1008'],
    ['id' => '100880', 'name' => 'PEKAN HICOM', 'district_id' => '1008'],
    ['id' => '100881', 'name' => 'PEKAN KAYU ARA', 'district_id' => '1008'],
    ['id' => '100882', 'name' => 'PEKAN KINRARA', 'district_id' => '1008'],
    // ['id' => '100883', 'name' => '??', 'district_id' => '1008'], // deprecated?
    ['id' => '100884', 'name' => 'PEKAN BARU HICOM', 'district_id' => '1008'],
    ['id' => '100885', 'name' => 'PEKAN BARU SUBANG', 'district_id' => '1008'],
    ['id' => '100886', 'name' => 'PEKAN BARU SUNGAI BESI', 'district_id' => '1008'],
    ['id' => '100887', 'name' => 'PEKAN BARU SUNGAI BULOH', 'district_id' => '1008'],
    ['id' => '100888', 'name' => 'PEKAN BARU PENAGA', 'district_id' => '1008'],
    ['id' => '100889', 'name' => 'PEKAN PUCHONG JAYA', 'district_id' => '1008'],
    ['id' => '100890', 'name' => 'PEKAN PUCHONG PERDANA', 'district_id' => '1008'],
    ['id' => '100891', 'name' => 'PEKAN SUBANG', 'district_id' => '1008'],
    ['id' => '100892', 'name' => 'PEKAN SUBANG JAYA', 'district_id' => '1008'],
    /*
    /*
     * === GOMBAK
     */
    // ['id' => '100901', 'name' => 'BATU', 'district_id' => '1009'], // deprecated?
    ['id' => '100902', 'name' => 'ULU KELANG', 'district_id' => '1009'],
    ['id' => '100903', 'name' => 'RAWANG', 'district_id' => '1009'],
    ['id' => '100904', 'name' => 'SETAPAK', 'district_id' => '1009'],

    ['id' => '100940', 'name' => 'BANDAR BATU ARANG', 'district_id' => '1009'],
    // ['id' => '100941', 'name' => '??', 'district_id' => '1009'], // deprecated?
    ['id' => '100942', 'name' => 'BANDAR RAWANG', 'district_id' => '1009'],
    ['id' => '100943', 'name' => 'BANDAR GOMBAK SETIA', 'district_id' => '1009'],
    ['id' => '100944', 'name' => 'BANDAR ULU KELANG', 'district_id' => '1009'],
    ['id' => '100945', 'name' => 'BANDAR KEPONG', 'district_id' => '1009'],
    ['id' => '100946', 'name' => 'BANDAR KUNDANG', 'district_id' => '1009'],
    ['id' => '100947', 'name' => 'BANDAR SELAYANG', 'district_id' => '1009'],
    ['id' => '100948', 'name' => 'BANDAR SUNGAI BULOH', 'district_id' => '1009'],
    ['id' => '100949', 'name' => 'BANDAR SUNGAI PUSU', 'district_id' => '1009'],

    // ['id' => '100970', 'name' => '??', 'district_id' => '1009'], // deprecated?
    // ['id' => '100971', 'name' => '??', 'district_id' => '1009'], // deprecated?
    ['id' => '100972', 'name' => 'PEKAN BATU 20', 'district_id' => '1009'],
    ['id' => '100973', 'name' => 'PEKAN KUANG', 'district_id' => '1009'],
    ['id' => '100974', 'name' => 'PEKAN MIMALAND', 'district_id' => '1009'],
    ['id' => '100975', 'name' => 'PEKAN PENGKALAN KUNDANG', 'district_id' => '1009'],
    ['id' => '100976', 'name' => 'PEKAN SUNGAI BULOH', 'district_id' => '1009'],
    ['id' => '100977', 'name' => 'PEKAN TEMPLER', 'district_id' => '1009'],
    /*
     * === SEPANG
     */
    ['id' => '101001', 'name' => 'DENGKIL', 'district_id' => '1010'],
    // ['id' => '101002', 'name' => 'LABU', 'district_id' => '1010'], // deprecated?
    ['id' => '101003', 'name' => 'SEPANG', 'district_id' => '1010'],

    ['id' => '101040', 'name' => 'BANDAR SEPANG', 'district_id' => '1010'],
    ['id' => '101041', 'name' => 'BANDAR BARU BANGI', 'district_id' => '1010'],
    ['id' => '101042', 'name' => 'BANDAR BARU SALAK TINGGI', 'district_id' => '1010'],
    ['id' => '101043', 'name' => 'BANDAR LAPANGAN TERBANG ANTARABANGSA SEPANG', 'district_id' => '1010'],
    ['id' => '101044', 'name' => 'BANDAR SUNGAI MERAB', 'district_id' => '1010'],

    ['id' => '101070', 'name' => 'PEKAN DENGKIL', 'district_id' => '1010'],
    ['id' => '101071', 'name' => 'PEKAN SALAK', 'district_id' => '1010'],
    ['id' => '101072', 'name' => 'PEKAN SUNGAI PELEK', 'district_id' => '1010'],
    // ['id' => '101073', 'name' => '??', 'district_id' => '1010'], // deprecated?
    ['id' => '101074', 'name' => 'PEKAN BATU 1 SEPANG', 'district_id' => '1010'],
    ['id' => '101075', 'name' => 'PEKAN BUKIT BISA', 'district_id' => '1010'],
    ['id' => '101076', 'name' => 'PEKAN BUKIT PRANG', 'district_id' => '1010'],
    ['id' => '101077', 'name' => 'PEKAN DATO BAKAR BAGINDA', 'district_id' => '1010'],
    ['id' => '101078', 'name' => 'PEKAN BARU SALAK TINGGI', 'district_id' => '1010'],
    ['id' => '101079', 'name' => 'PEKAN SUNGAI MERAB', 'district_id' => '1010'],
    ['id' => '101080', 'name' => 'PEKAN TANJUNG MAS', 'district_id' => '1010'],
    /*
     * KUALA LUMPUR
     */
    ['id' => '140101', 'name' => 'AMPANG', 'district_id' => '1401'],
    ['id' => '140102', 'name' => 'BANDAR KUALA LUMPUR', 'district_id' => '1401'],
    ['id' => '140103', 'name' => 'BATU', 'district_id' => '1401'],
    ['id' => '140104', 'name' => 'CHERAS', 'district_id' => '1401'],
    ['id' => '140105', 'name' => 'KUALA LUMPUR', 'district_id' => '1401'],
    ['id' => '140106', 'name' => 'PETALING', 'district_id' => '1401'],
    ['id' => '140107', 'name' => 'SETAPAK', 'district_id' => '1401'],
    ['id' => '140108', 'name' => 'ULU KELANG', 'district_id' => '1401'],
    /*
     * PUTRAJAYA
     */
    ['id' => '160101', 'name' => 'PUTRAJAYA', 'district_id' => '1601'],
    /*
     * negeri sembilan
     */
    /*
     * === JELEBU
     */
    ['id' => '050101', 'name' => 'GELAMI LEMI', 'district_id' => '0501'],
    ['id' => '050102', 'name' => 'KENABOI', 'district_id' => '0501'],
    ['id' => '050103', 'name' => 'KUALA KELAWANG', 'district_id' => '0501'],
    ['id' => '050104', 'name' => 'PERADONG', 'district_id' => '0501'],
    ['id' => '050105', 'name' => 'PERTANG', 'district_id' => '0501'],
    ['id' => '050106', 'name' => 'TERIANG HILIR', 'district_id' => '0501'],
    ['id' => '050107', 'name' => 'HULU KELAWANG', 'district_id' => '0501'],
    ['id' => '050108', 'name' => 'HULU TERIANG', 'district_id' => '0501'],
    /*
     * === KUALA PILAH
     */
    ['id' => '050201', 'name' => 'AMPANG TINGGI', 'district_id' => '0502'],
    ['id' => '050202', 'name' => 'JOHOL', 'district_id' => '0502'],
    ['id' => '050203', 'name' => 'JUASSEH', 'district_id' => '0502'],
    ['id' => '050204', 'name' => 'KEPIS', 'district_id' => '0502'],
    ['id' => '050205', 'name' => 'LANGKAP', 'district_id' => '0502'],
    ['id' => '050206', 'name' => 'PARIT TINGGI', 'district_id' => '0502'],
    ['id' => '050207', 'name' => 'PILAH', 'district_id' => '0502'],
    ['id' => '050208', 'name' => 'SRI MENANTI', 'district_id' => '0502'],
    ['id' => '050209', 'name' => 'TERACHI', 'district_id' => '0502'],
    ['id' => '050210', 'name' => 'ULU JEMPOL', 'district_id' => '0502'],
    ['id' => '050211', 'name' => 'ULU MUAR', 'district_id' => '0502'],
    /*
     * === PORT DICKSON
     */
    ['id' => '050301', 'name' => 'JIMAH', 'district_id' => '0503'],
    ['id' => '050302', 'name' => 'LINGGI', 'district_id' => '0503'],
    ['id' => '050303', 'name' => 'PASIR PANJANG', 'district_id' => '0503'],
    ['id' => '050304', 'name' => 'PORT DICKSON', 'district_id' => '0503'],
    ['id' => '050305', 'name' => 'SI RUSA', 'district_id' => '0503'],
    /*
     * === REMBAU
     */
    ['id' => '050401', 'name' => 'BATU HAMPAR', 'district_id' => '0504'],
    ['id' => '050402', 'name' => 'BONGEK', 'district_id' => '0504'],
    ['id' => '050403', 'name' => 'CHEMBONG', 'district_id' => '0504'],
    ['id' => '050404', 'name' => 'CHENGKAU', 'district_id' => '0504'],
    ['id' => '050405', 'name' => 'GADONG', 'district_id' => '0504'],
    ['id' => '050406', 'name' => 'KUNDOR', 'district_id' => '0504'],
    ['id' => '050407', 'name' => 'LEGONG HILIR', 'district_id' => '0504'],
    ['id' => '050408', 'name' => 'LEGONG HULU', 'district_id' => '0504'],
    ['id' => '050409', 'name' => 'MIKU', 'district_id' => '0504'],
    ['id' => '050410', 'name' => 'NERASAU', 'district_id' => '0504'],
    ['id' => '050411', 'name' => 'PEDAS', 'district_id' => '0504'],
    ['id' => '050412', 'name' => 'PILIN', 'district_id' => '0504'],
    ['id' => '050413', 'name' => 'SELEMAK', 'district_id' => '0504'],
    ['id' => '050414', 'name' => 'SEMERBOK', 'district_id' => '0504'],
    ['id' => '050415', 'name' => 'SPRI', 'district_id' => '0504'],
    ['id' => '050416', 'name' => 'TANJONG KELING', 'district_id' => '0504'],
    ['id' => '050417', 'name' => 'TITIAN BINTANGOR', 'district_id' => '0504'],
    /*
     * === SEREMBAN
     */
    ['id' => '050501', 'name' => 'AMPANGAN', 'district_id' => '0505'],
    ['id' => '050502', 'name' => 'LABU', 'district_id' => '0505'],
    ['id' => '050503', 'name' => 'LENGGENG', 'district_id' => '0505'],
    ['id' => '050504', 'name' => 'PANTAI', 'district_id' => '0505'],
    ['id' => '050505', 'name' => 'RANTAU', 'district_id' => '0505'],
    ['id' => '050506', 'name' => 'RASAH', 'district_id' => '0505'],
    ['id' => '050507', 'name' => 'SEREMBAN', 'district_id' => '0505'],
    ['id' => '050508', 'name' => 'SETUL', 'district_id' => '0505'],
    ['id' => '050509', 'name' => 'BANDAR SEREMBAN', 'district_id' => '0505'],
    /*
     * === TAMPIN
     */
    ['id' => '050601', 'name' => 'AYER KUNING', 'district_id' => '0506'],
    ['id' => '050602', 'name' => 'GEMAS', 'district_id' => '0506'],
    ['id' => '050603', 'name' => 'GEMENCHEH', 'district_id' => '0506'],
    ['id' => '050604', 'name' => 'KERU', 'district_id' => '0506'],
    ['id' => '050605', 'name' => 'REPAH', 'district_id' => '0506'],
    ['id' => '050606', 'name' => 'TAMPIN TENGAH', 'district_id' => '0506'],
    ['id' => '050607', 'name' => 'TEBONG', 'district_id' => '0506'],
    /*
     * === JEMPOL
     */
    ['id' => '050701', 'name' => 'JELAI', 'district_id' => '0507'],
    ['id' => '050702', 'name' => 'KUALA JEMPOL', 'district_id' => '0507'],
    ['id' => '050703', 'name' => 'ROMPIN', 'district_id' => '0507'],
    ['id' => '050704', 'name' => 'SERTING HILIR', 'district_id' => '0507'],
    ['id' => '050705', 'name' => 'SERTING ULU', 'district_id' => '0507'],

    /*
     * melaka
     */
    /*
     * === MELAKA TENGAH
     */
    ['id' => '040101', 'name' => 'ALAI', 'district_id' => '0401'],
    ['id' => '040102', 'name' => 'AYER MOLEK', 'district_id' => '0401'],
    ['id' => '040103', 'name' => 'BACHANG', 'district_id' => '0401'],
    ['id' => '040104', 'name' => 'BALAI PANJANG', 'district_id' => '0401'],
    ['id' => '040105', 'name' => 'BATU BERENDAM', 'district_id' => '0401'],
    ['id' => '040106', 'name' => 'BERTAM', 'district_id' => '0401'],
    ['id' => '040107', 'name' => 'BUKIT BARU', 'district_id' => '0401'],
    ['id' => '040108', 'name' => 'BUKIT KATIL', 'district_id' => '0401'],
    ['id' => '040109', 'name' => 'BUKIT LINTANG', 'district_id' => '0401'],
    ['id' => '040110', 'name' => 'BUKIT PIATU', 'district_id' => '0401'],
    ['id' => '040111', 'name' => 'BUKIT RAMBAI', 'district_id' => '0401'],
    ['id' => '040112', 'name' => 'CHENG', 'district_id' => '0401'],
    ['id' => '040113', 'name' => 'DUYONG', 'district_id' => '0401'],
    ['id' => '040114', 'name' => 'KANDANG', 'district_id' => '0401'],
    ['id' => '040115', 'name' => 'KLEBANG BESAR', 'district_id' => '0401'],
    ['id' => '040116', 'name' => 'KLEBANG KECHIL', 'district_id' => '0401'],
    ['id' => '040117', 'name' => 'KRUBONG', 'district_id' => '0401'],
    ['id' => '040118', 'name' => 'PADANG SEMABOK', 'district_id' => '0401'],
    ['id' => '040119', 'name' => 'PADANG TEMU', 'district_id' => '0401'],
    ['id' => '040120', 'name' => 'PAYA RUMPUT', 'district_id' => '0401'],
    ['id' => '040121', 'name' => 'PRINGGIT', 'district_id' => '0401'],
    ['id' => '040122', 'name' => 'PERNU', 'district_id' => '0401'],
    ['id' => '040123', 'name' => 'SEMABOK', 'district_id' => '0401'],
    ['id' => '040124', 'name' => 'SUNGAI UDANG', 'district_id' => '0401'],
    ['id' => '040125', 'name' => 'TANGGA BATU', 'district_id' => '0401'],
    ['id' => '040126', 'name' => 'TANJONG KELING', 'district_id' => '0401'],
    ['id' => '040127', 'name' => 'TANJONG MINYAK', 'district_id' => '0401'],
    ['id' => '040128', 'name' => 'TELOK MAS', 'district_id' => '0401'],
    ['id' => '040129', 'name' => 'UJONG PASIR', 'district_id' => '0401'],
    ['id' => '040130', 'name' => 'BANDAR MELAKA', 'district_id' => '0401'],
    /*
     * === JASIN
     */
    ['id' => '040201', 'name' => 'AYER PANAS', 'district_id' => '0402'],
    ['id' => '040202', 'name' => 'BATANG MALAKA', 'district_id' => '0402'],
    ['id' => '040203', 'name' => 'BUKIT SENGGEH', 'district_id' => '0402'],
    ['id' => '040204', 'name' => 'CHABAU', 'district_id' => '0402'],
    ['id' => '040205', 'name' => 'CHIN CHIN', 'district_id' => '0402'],
    ['id' => '040206', 'name' => 'CHOHONG', 'district_id' => '0402'],
    ['id' => '040207', 'name' => 'JASIN', 'district_id' => '0402'],
    ['id' => '040208', 'name' => 'JUS', 'district_id' => '0402'],
    ['id' => '040209', 'name' => 'KESANG', 'district_id' => '0402'],
    ['id' => '040210', 'name' => 'MERLIMAU', 'district_id' => '0402'],
    ['id' => '040211', 'name' => 'NYALAS', 'district_id' => '0402'],
    ['id' => '040212', 'name' => 'RIM', 'district_id' => '0402'],
    ['id' => '040213', 'name' => 'SEBATU', 'district_id' => '0402'],
    ['id' => '040214', 'name' => 'SELANDAR', 'district_id' => '0402'],
    ['id' => '040215', 'name' => 'SEMPANG', 'district_id' => '0402'],
    ['id' => '040216', 'name' => 'SEMUJOK', 'district_id' => '0402'],
    ['id' => '040217', 'name' => 'SERKAM', 'district_id' => '0402'],
    ['id' => '040218', 'name' => 'SUNGEI RAMBAI', 'district_id' => '0402'],
    ['id' => '040219', 'name' => 'TEDONG', 'district_id' => '0402'],
    ['id' => '040220', 'name' => 'UMBAI', 'district_id' => '0402'],
    /*
     * === ALOR GAJAR
     */
    ['id' => '040301', 'name' => "AYER PA'ABAS", 'district_id' => '0403'],
    ['id' => '040302', 'name' => 'BELIMBING', 'district_id' => '0403'],
    ['id' => '040303', 'name' => 'BERINGIN', 'district_id' => '0403'],
    ['id' => '040304', 'name' => 'BRISU', 'district_id' => '0403'],
    ['id' => '040305', 'name' => 'TANJUNG TUAN (CAPE RACHADO)', 'district_id' => '0403'],
    ['id' => '040306', 'name' => 'DURIAN TUNGGAL', 'district_id' => '0403'],
    ['id' => '040307', 'name' => 'GADEK', 'district_id' => '0403'],
    ['id' => '040308', 'name' => 'KELEMAK', 'district_id' => '0403'],
    ['id' => '040309', 'name' => 'KEMUNING', 'district_id' => '0403'],
    ['id' => '040310', 'name' => 'KUALA LINGGI', 'district_id' => '0403'],
    ['id' => '040311', 'name' => 'KUALA SUNGEI BARU', 'district_id' => '0403'],
    ['id' => '040312', 'name' => 'LENDU', 'district_id' => '0403'],
    ['id' => '040313', 'name' => 'MACHAP', 'district_id' => '0403'],
    ['id' => '040314', 'name' => 'MASJID TANAH', 'district_id' => '0403'],
    ['id' => '040315', 'name' => 'MELAKA PINDAH', 'district_id' => '0403'],
    ['id' => '040316', 'name' => 'MELEKEK', 'district_id' => '0403'],
    ['id' => '040317', 'name' => 'PADANG SEBANG', 'district_id' => '0403'],
    ['id' => '040318', 'name' => 'PARIT MELANA', 'district_id' => '0403'],
    ['id' => '040319', 'name' => 'PEGOH', 'district_id' => '0403'],
    ['id' => '040320', 'name' => 'PULAU SEBANG', 'district_id' => '0403'],
    ['id' => '040321', 'name' => 'RAMUAN CHINA BESAR', 'district_id' => '0403'],
    ['id' => '040322', 'name' => 'RAMUAN CHINA KECHIL', 'district_id' => '0403'],
    ['id' => '040323', 'name' => 'REMBIA', 'district_id' => '0403'],
    ['id' => '040324', 'name' => 'SUNGEI BARU HILIR', 'district_id' => '0403'],
    ['id' => '040325', 'name' => 'SUNGEI BARU TENGAH', 'district_id' => '0403'],
    ['id' => '040326', 'name' => 'SUNGEI BARU ULU', 'district_id' => '0403'],
    ['id' => '040327', 'name' => 'SUNGEI BULOH', 'district_id' => '0403'],
    ['id' => '040328', 'name' => 'SUNGEI PETAI', 'district_id' => '0403'],
    ['id' => '040329', 'name' => 'SUNGEI SIPUT', 'district_id' => '0403'],
    ['id' => '040330', 'name' => 'TABOH NANING', 'district_id' => '0403'],
    ['id' => '040331', 'name' => 'TANJONG RIMAU', 'district_id' => '0403'],
    ['id' => '040332', 'name' => 'TEBONG', 'district_id' => '0403'],
    /*
     * johor
     */
    /*
     * === batu pahat
     */
    ['id' => '010101', 'name' => 'BAGAN', 'district_id' => '0101'],
    ['id' => '010102', 'name' => 'CHAAH BAHRU', 'district_id' => '0101'],
    ['id' => '010103', 'name' => 'KAMPONG BAHRU', 'district_id' => '0101'],
    ['id' => '010104', 'name' => 'LINAU', 'district_id' => '0101'],
    ['id' => '010105', 'name' => 'LUBOK', 'district_id' => '0101'],
    ['id' => '010106', 'name' => 'MINYAK BEKU', 'district_id' => '0101'],
    ['id' => '010107', 'name' => 'PESERAI', 'district_id' => '0101'],
    ['id' => '010108', 'name' => 'SIMPANG KANAN', 'district_id' => '0101'],
    ['id' => '010109', 'name' => 'SIMPANG KIRI', 'district_id' => '0101'],
    ['id' => '010110', 'name' => 'SRI GADING', 'district_id' => '0101'],
    ['id' => '010111', 'name' => 'SRI MEDAN', 'district_id' => '0101'],
    ['id' => '010112', 'name' => 'SUNGAI KLUANG', 'district_id' => '0101'],
    ['id' => '010113', 'name' => 'SUNGAI PUNGGOR', 'district_id' => '0101'],
    ['id' => '010114', 'name' => 'TANJONG SEMBRONG', 'district_id' => '0101'],
    /*
     * === johor bahru
     */
    ['id' => '010201', 'name' => 'JELUTONG', 'district_id' => '0102'],
    ['id' => '010202', 'name' => 'PLENTONG', 'district_id' => '0102'],
    ['id' => '010203', 'name' => 'PULAI', 'district_id' => '0102'],
    // ['id' => '010204', 'name' => 'SEDENAK', 'district_id' => '0102'], //change to kulaijaya since 2008
    // ['id' => '010205', 'name' => 'SENAI / KULAI', 'district_id' => '0102'], //change to kulaijaya since 2008
    ['id' => '010206', 'name' => 'SUNGAI TIRAM', 'district_id' => '0102'],
    ['id' => '010207', 'name' => 'TANJUNG KUPANG', 'district_id' => '0102'],
    ['id' => '010208', 'name' => 'TEMBRAU', 'district_id' => '0102'],
    ['id' => '010209', 'name' => 'BANDAR JOHOR BAHRU', 'district_id' => '0102'],
    /*
     * === kluang
     */
    ['id' => '010301', 'name' => 'ULU BENUT', 'district_id' => '0103'],
    ['id' => '010302', 'name' => 'KAHANG', 'district_id' => '0103'],
    ['id' => '010303', 'name' => 'KLUANG', 'district_id' => '0103'],
    ['id' => '010304', 'name' => 'LAYANG-LAYANG', 'district_id' => '0103'],
    ['id' => '010305', 'name' => 'MACHAP', 'district_id' => '0103'],
    ['id' => '010306', 'name' => 'NIYOR', 'district_id' => '0103'],
    ['id' => '010307', 'name' => 'PALOH', 'district_id' => '0103'],
    ['id' => '010308', 'name' => 'RENGAM', 'district_id' => '0103'],
    /*
     * === kota tinggi
     */
    ['id' => '010401', 'name' => 'JOHOR LAMA', 'district_id' => '0104'],
    ['id' => '010402', 'name' => 'KAMBAU', 'district_id' => '0104'],
    ['id' => '010403', 'name' => 'KOTA TINGGI', 'district_id' => '0104'],
    ['id' => '010404', 'name' => 'PANTAI TIMUR', 'district_id' => '0104'],
    ['id' => '010405', 'name' => 'PENGGERANG', 'district_id' => '0104'],
    ['id' => '010406', 'name' => 'SELIDI BESAR', 'district_id' => '0104'],
    ['id' => '010407', 'name' => 'SELIDI KECHIL', 'district_id' => '0104'],
    ['id' => '010408', 'name' => 'TANJUNG SURAT', 'district_id' => '0104'],
    ['id' => '010409', 'name' => 'ULU SUNGAI JOHOR', 'district_id' => '0104'],
    ['id' => '010410', 'name' => 'ULU SUNGAI SELIDI BESAR', 'district_id' => '0104'],
    /*
     * === mersing
     */
    ['id' => '010501', 'name' => 'JEMALUANG', 'district_id' => '0105'],
    ['id' => '010502', 'name' => 'LENGGOR', 'district_id' => '0105'],
    ['id' => '010503', 'name' => 'MERSING', 'district_id' => '0105'],
    ['id' => '010504', 'name' => 'PADANG ENDAU', 'district_id' => '0105'],
    ['id' => '010505', 'name' => 'PENYABONG', 'district_id' => '0105'],
    ['id' => '010506', 'name' => 'PULAU AUR', 'district_id' => '0105'],
    ['id' => '010507', 'name' => 'PULAU BABI', 'district_id' => '0105'],
    ['id' => '010508', 'name' => 'PULAU PEMANGGIL', 'district_id' => '0105'],
    ['id' => '010509', 'name' => 'PULAU SIBU', 'district_id' => '0105'],
    ['id' => '010510', 'name' => 'PULAU TINGGI', 'district_id' => '0105'],
    ['id' => '010511', 'name' => 'SEMBRONG', 'district_id' => '0105'],
    ['id' => '010512', 'name' => 'TENGGAROH', 'district_id' => '0105'],
    ['id' => '010513', 'name' => 'TENGLU', 'district_id' => '0105'],
    ['id' => '010514', 'name' => 'TRIANG', 'district_id' => '0105'],
    /*
     * === muar
     */
    ['id' => '010501', 'name' => 'AYER HITAM', 'district_id' => '0106'],
    ['id' => '010602', 'name' => 'BANDAR', 'district_id' => '0106'],
    ['id' => '010603', 'name' => 'BUKIT KEPONG', 'district_id' => '0106'],
    // ['id' => '010604', 'name' => 'BUKIT SERAMPANG', 'district_id' => '0106'], //tukar ke ledang since 2008
    // ['id' => '010605', 'name' => 'GRISEK', 'district_id' => '0106'], //tukar ke ledang since 2008
    ['id' => '010606', 'name' => 'JALAN BAKRI', 'district_id' => '0106'],
    ['id' => '010607', 'name' => 'JORAK', 'district_id' => '0106'],
    // ['id' => '010608', 'name' => 'KESANG', 'district_id' => '0106'], //tukar ke ledang since 2008
    // ['id' => '010609', 'name' => 'KUNDANG', 'district_id' => '0106'], //tukar ke ledang since 2008
    ['id' => '010610', 'name' => 'LENGA', 'district_id' => '0106'],
    ['id' => '010611', 'name' => 'PARIT BAKAR', 'district_id' => '0106'],
    ['id' => '010612', 'name' => 'PARIT JAWA', 'district_id' => '0106'],
    // ['id' => '010613', 'name' => 'SEROM', 'district_id' => '0106'], //tukar ke ledang since 2008
    ['id' => '010614', 'name' => 'SRI MERANTI', 'district_id' => '0106'],
    ['id' => '010615', 'name' => 'SUNGAI BALANG', 'district_id' => '0106'],
    ['id' => '010616', 'name' => 'SUNGAI RAYA & KAMPUNG BUKIT PASIR', 'district_id' => '0106'],
    ['id' => '010617', 'name' => 'SUNGAI TERAP', 'district_id' => '0106'],
    // ['id' => '010618', 'name' => 'TANGKAK', 'district_id' => '0106'], //tukar ke ledang since 2008
    /*
     * === pontian
     */
    ['id' => '010701', 'name' => 'API-API', 'district_id' => '0107'],
    ['id' => '010702', 'name' => 'AYER BALOI', 'district_id' => '0107'],
    ['id' => '010703', 'name' => 'AIR MASIN', 'district_id' => '0107'],
    ['id' => '010704', 'name' => 'BENUT', 'district_id' => '0107'],
    ['id' => '010705', 'name' => 'JERAM BATU', 'district_id' => '0107'],
    ['id' => '010706', 'name' => 'PONTIAN', 'district_id' => '0107'],
    ['id' => '010707', 'name' => 'RIMBA TERJUN', 'district_id' => '0107'],
    ['id' => '010708', 'name' => 'SERKAT', 'district_id' => '0107'],
    ['id' => '010709', 'name' => 'SUNGAI KARANG', 'district_id' => '0107'],
    ['id' => '010710', 'name' => 'SUNGEI PINGGAN', 'district_id' => '0107'],
    ['id' => '010711', 'name' => 'PENGKALAN RAJA', 'district_id' => '0107'],
    /*
     * === segamat
     */
    ['id' => '010801', 'name' => 'BEKOK', 'district_id' => '0108'],
    ['id' => '010802', 'name' => 'BULOH KASAP', 'district_id' => '0108'],
    ['id' => '010803', 'name' => 'CHAAH', 'district_id' => '0108'],
    ['id' => '010804', 'name' => 'GEMAS', 'district_id' => '0108'],
    ['id' => '010805', 'name' => 'GEMEREH', 'district_id' => '0108'],
    ['id' => '010806', 'name' => 'JABI', 'district_id' => '0108'],
    ['id' => '010807', 'name' => 'JEMENTAH', 'district_id' => '0108'],
    ['id' => '010808', 'name' => 'LABIS', 'district_id' => '0108'],
    ['id' => '010809', 'name' => 'POGOH', 'district_id' => '0108'],
    ['id' => '010810', 'name' => 'SERMIN', 'district_id' => '0108'],
    ['id' => '010811', 'name' => 'SUNGAI SEGAMAT', 'district_id' => '0108'],
    ['id' => '010812', 'name' => 'BANDAR SEGAMAT', 'district_id' => '0108'],
    /*
     * === kulaijaya // upgrade from kulai since 2008 - http://www.utusan.com.my/utusan/info.asp?y=2008&dt=0622&pub=Utusan_Malaysia&sec=Johor&pg=wj_01.htm
     */
    ['id' => '010901', 'name' => 'SENAI', 'district_id' => '0109'],
    ['id' => '010902', 'name' => 'KULAI', 'district_id' => '0109'],
    ['id' => '010903', 'name' => 'BUKIT BATU', 'district_id' => '0109'],
    ['id' => '010904', 'name' => 'SEDENAK', 'district_id' => '0109'],
    /*
     * === ledang // upgrade since 2008
     */
    ['id' => '011001', 'name' => 'TANGKAK', 'district_id' => '0110'],
    ['id' => '011002', 'name' => 'SEROM', 'district_id' => '0110'],
    ['id' => '011003', 'name' => 'GERSIK', 'district_id' => '0110'],
    ['id' => '011004', 'name' => 'BUKIT SERAMPANG', 'district_id' => '0110'],
    ['id' => '011005', 'name' => 'KUNDANG', 'district_id' => '0110'],
    ['id' => '011006', 'name' => 'KESANG', 'district_id' => '0110'],
    /*
     * labuan
     */
    ['id' => '150101', 'name' => 'LABUAN', 'district_id' => '1501'],
    /*
     * sabah
     */
    /*
     * === KOTA KINABALU
     */
    ['id' => '120101', 'name' => 'INANAM', 'district_id' => '1201'],
    ['id' => '120102', 'name' => 'KEPAYAN', 'district_id' => '1201'],
    ['id' => '120103', 'name' => 'LIKAS', 'district_id' => '1201'],
    ['id' => '120104', 'name' => 'LOK KAWI', 'district_id' => '1201'],
    ['id' => '120105', 'name' => 'MENGGATAL', 'district_id' => '1201'],
    ['id' => '120106', 'name' => 'SEPANGGAR', 'district_id' => '1201'],
    ['id' => '120107', 'name' => 'TANJUNG ARU', 'district_id' => '1201'],
    ['id' => '120108', 'name' => 'TELIPOK', 'district_id' => '1201'],
    /*
     * === PAPAR
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === KOTA BELUD
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === TUARAN http://www.sabah.gov.my/pd.trn/mukim.html
     */
    ['id' => '120401', 'name' => 'BERUNGIS', 'district_id' => '1204'],
    ['id' => '120402', 'name' => 'INDAI', 'district_id' => '1204'],
    ['id' => '120403', 'name' => 'LEMBAH', 'district_id' => '1204'],
    ['id' => '120404', 'name' => 'MENGKABONG', 'district_id' => '1204'],
    ['id' => '120405', 'name' => 'NABALU', 'district_id' => '1204'],
    ['id' => '120406', 'name' => 'PANTAI', 'district_id' => '1204'],
    ['id' => '120407', 'name' => 'PEKAN', 'district_id' => '1204'],
    ['id' => '120408', 'name' => 'SERUSOP', 'district_id' => '1204'],
    ['id' => '120409', 'name' => 'TAMBALANG', 'district_id' => '1204'],
    ['id' => '120410', 'name' => 'TENGAH', 'district_id' => '1204'],
    ['id' => '120411', 'name' => 'ULU', 'district_id' => '1204'],
    ['id' => '120412', 'name' => 'MANGKALADOI', 'district_id' => '1204'],
    ['id' => '120413', 'name' => 'TENGHILAN', 'district_id' => '1204'],
    ['id' => '120414', 'name' => 'TOPOKON', 'district_id' => '1204'],
    ['id' => '120415', 'name' => 'TUARAN BANDAR', 'district_id' => '1204'],
    ['id' => '120416', 'name' => 'TAMPARULI', 'district_id' => '1204'],
    /*
     * === KUDAT
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === RANAU
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === SANDAKAN
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === LABUK & SUGUT
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === KINABATANGAN
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === TAWAU
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === LAHAD DATU
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === SEMPORNA
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === KENINGAU
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === TAMBUNAN
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === PENSIANGAN
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === TENOM
     */
    ['id' => '020301', 'name' => '', 'district_id' => '0203'],
    /*
     * === BEAUFORT
     */
    ['id' => '121701', 'name' => 'MEMBAKUT', 'district_id' => '1217'],
    /*
     * === KUALA PENYU
     */
    ['id' => '121801', 'name' => '', 'district_id' => '1218'],
    /*
     * === SIPITANG
     */
    ['id' => '020301', 'name' => '', 'district_id' => '1219'],
    /*
     * === PENAMPANG
     */
    ['id' => '122001', 'name' => 'DONGGONGON', 'district_id' => '1220'], //?
    /*
     * === KOTA MARUDU
     */
    ['id' => '020301', 'name' => '', 'district_id' => '1221'],
    /*
     * === KUNAK
     */
    ['id' => '020301', 'name' => '', 'district_id' => '1222'],
    /*
     * === TONGOD
     */
    ['id' => '020301', 'name' => '', 'district_id' => '1223'],
    /*
     * === PUTATAN
     */
    ['id' => '020301', 'name' => '', 'district_id' => '1224'],
    /*
     * sarawak
     */
    /*
     * === KUCHING
     */
    ['id' => '130101', 'name' => 'PUEH LAND', 'district_id' => '1301'],
    ['id' => '130102', 'name' => 'GADING LUNDU', 'district_id' => '1301'],
    ['id' => '130103', 'name' => 'STUNGKOR', 'district_id' => '1301'],
    ['id' => '130104', 'name' => 'SAMPADI', 'district_id' => '1301'],
    ['id' => '130105', 'name' => 'JAGOI', 'district_id' => '1301'],
    ['id' => '130106', 'name' => 'SENGGI POAK', 'district_id' => '1301'],
    ['id' => '130107', 'name' => 'MATANG', 'district_id' => '1301'],
    ['id' => '130108', 'name' => 'SALAK', 'district_id' => '1301'],
    ['id' => '130109', 'name' => 'PANGKALAN AMPAT', 'district_id' => '1301'],
    ['id' => '130110', 'name' => 'KUCHING UTARA', 'district_id' => '1301'],
    ['id' => '130111', 'name' => 'KUCHING TENGAH', 'district_id' => '1301'],
    ['id' => '130112', 'name' => 'BANDAR KUCHING', 'district_id' => '1301'],
    ['id' => '130113', 'name' => 'SENTAH-SEGU', 'district_id' => '1301'],
    ['id' => '130114', 'name' => 'MUARA TEBAS', 'district_id' => '1301'],
    ['id' => '130125', 'name' => 'BANDAR BATU KAWA', 'district_id' => '1301'],
    ['id' => '130126', 'name' => 'BATU 8, JALAN MATANG', 'district_id' => '1301'],
    ['id' => '130127', 'name' => 'BANDAR SUNGAI TENGAH', 'district_id' => '1301'],
    ['id' => '130128', 'name' => 'BANDAR BATU KITANG', 'district_id' => '1301'],
    ['id' => '130129', 'name' => 'BATU 15, JALAN SENGGANG', 'district_id' => '1301'],
    ['id' => '130130', 'name' => '175, JALAN SENGGANG', 'district_id' => '1301'],
    ['id' => '130131', 'name' => 'BANDAR JALAN BATU 19, JALAN SENGGANG', 'district_id' => '1301'],
    ['id' => '130132', 'name' => 'BANDAR BATU 24, JALAN SENGGANG', 'district_id' => '1301'],
    ['id' => '130133', 'name' => 'BANDAR PANGKALAN KUT', 'district_id' => '1301'],
    ['id' => '130149', 'name' => 'BANDAR BELIONG', 'district_id' => '1301'],
    ['id' => '130150', 'name' => 'BANDAR BAKO', 'district_id' => '1301'],
    ['id' => '130155', 'name' => 'BANDAR SEMATANG', 'district_id' => '1301'],
    ['id' => '130156', 'name' => 'BANDAR LUNDU', 'district_id' => '1301'],
    ['id' => '130157', 'name' => 'BANDAR JANGKAR', 'district_id' => '1301'],
    ['id' => '130158', 'name' => 'BANDAR RAMBUNGAN', 'district_id' => '1301'],
    ['id' => '130159', 'name' => 'BANDAR STUNKOR', 'district_id' => '1301'],
    ['id' => '130160', 'name' => 'BANDAR KRANJI', 'district_id' => '1301'],
    ['id' => '130161', 'name' => 'BANDAR SINIWAN', 'district_id' => '1301'],
    ['id' => '130162', 'name' => 'BANDAR PAKU', 'district_id' => '1301'],
    ['id' => '130163', 'name' => 'BANDAR JAMBUSAN', 'district_id' => '1301'],
    ['id' => '130164', 'name' => 'BANDAR BAU', 'district_id' => '1301'],
    ['id' => '130165', 'name' => 'BANDAR BUSO', 'district_id' => '1301'],
    ['id' => '130166', 'name' => 'BANDAR TUNDONG', 'district_id' => '1301'],
    ['id' => '130167', 'name' => 'BANDAR MUSI', 'district_id' => '1301'],
    ['id' => '130168', 'name' => 'BANDAR TAI TON', 'district_id' => '1301'],
    ['id' => '130169', 'name' => 'BANDAR BIDI', 'district_id' => '1301'],
    ['id' => '130170', 'name' => 'BANDAR KROKONG', 'district_id' => '1301'],
    ['id' => '130171', 'name' => 'BANDAR PANGKALAN TEBANG', 'district_id' => '1301'],
    ['id' => '130172', 'name' => 'BANDAR PEJIRU', 'district_id' => '1301'],
    ['id' => '130173', 'name' => 'BANDAR TIANG BEKAP', 'district_id' => '1301'],
    ['id' => '130174', 'name' => 'BANDAR BARATOK', 'district_id' => '1301'],
    ['id' => '130175', 'name' => 'BANDAR TAPAH', 'district_id' => '1301'],
    ['id' => '130176', 'name' => 'BANDAR SIBURAN', 'district_id' => '1301'],
    ['id' => '130177', 'name' => 'BANDAR TERBAT', 'district_id' => '1301'],
    ['id' => '130178', 'name' => 'BANDAR JALAN BATU 17, JALAN SENGGANG', 'district_id' => '1301'],
    /*
     * === SRI AMAN
     */
    ['id' => '130201', 'name' => 'UNDUP', 'district_id' => '1302'],
    ['id' => '130202', 'name' => 'KLAUH', 'district_id' => '1302'],
    ['id' => '130203', 'name' => 'BIJAT', 'district_id' => '1302'],
    ['id' => '130204', 'name' => 'SKARANG', 'district_id' => '1302'],
    ['id' => '130205', 'name' => 'KERANGGAS', 'district_id' => '1302'],
    ['id' => '130206', 'name' => 'MARUP', 'district_id' => '1302'],
    ['id' => '130207', 'name' => 'LAMANAK', 'district_id' => '1302'],
    ['id' => '130208', 'name' => 'BUKIT BESAI', 'district_id' => '1302'],
    ['id' => '130209', 'name' => 'AI ENGKARI', 'district_id' => '1302'],
    ['id' => '130210', 'name' => 'LESONG', 'district_id' => '1302'],
    ['id' => '130211', 'name' => 'SELANJANG', 'district_id' => '1302'],
    ['id' => '130212', 'name' => 'SILANTEK', 'district_id' => '1302'],
    ['id' => '130224', 'name' => 'SIRNANGGANG', 'district_id' => '1302'],
    ['id' => '130225', 'name' => 'BANDAR LINGGA', 'district_id' => '1302'],
    ['id' => '130227', 'name' => 'BANDAR LUBOK ANTU', 'district_id' => '1302'],
    ['id' => '130228', 'name' => 'BANDAR ENGKILILI', 'district_id' => '1302'],
    ['id' => '130236', 'name' => 'BANDAR BATU LINTANG', 'district_id' => '1302'],
    ['id' => '130244', 'name' => 'BANDAR BANTING', 'district_id' => '1302'],
    ['id' => '130245', 'name' => 'BANDAR PANTU', 'district_id' => '1302'],
    ['id' => '130250', 'name' => 'BANDAR BAKONG', 'district_id' => '1302'],
    ['id' => '130252', 'name' => 'BANDAR UNDUP', 'district_id' => '1302'],
    ['id' => '130271', 'name' => 'BANDAR SKRANG', 'district_id' => '1302'],
    ['id' => '130272', 'name' => 'BANDAR MELUGU', 'district_id' => '1302'],
    ['id' => '130273', 'name' => 'BANDAR SABU', 'district_id' => '1302'],
    /*
     * === SIBU
     */
    ['id' => '130301', 'name' => 'SEDUAN', 'district_id' => '1303'],
    ['id' => '130302', 'name' => 'ENGKILO', 'district_id' => '1303'],
    ['id' => '130303', 'name' => 'PASAI-SIONG', 'district_id' => '1303'],
    ['id' => '130304', 'name' => 'ASSAN', 'district_id' => '1303'],
    ['id' => '130305', 'name' => 'MENYAN', 'district_id' => '1303'],
    ['id' => '130306', 'name' => 'KABANG', 'district_id' => '1303'],
    ['id' => '130319', 'name' => 'LUKUT', 'district_id' => '1303'],
    ['id' => '130320', 'name' => 'MAPAI', 'district_id' => '1303'],
    ['id' => '130321', 'name' => 'MAROH', 'district_id' => '1303'],
    ['id' => '130322', 'name' => 'SPALI', 'district_id' => '1303'],
    ['id' => '130337', 'name' => 'QYA-DALAT', 'district_id' => '1303'],
    ['id' => '130338', 'name' => 'SPAPA', 'district_id' => '1303'],
    ['id' => '130339', 'name' => 'PAKU', 'district_id' => '1303'],
    ['id' => '130340', 'name' => 'LALAI', 'district_id' => '1303'],
    ['id' => '130341', 'name' => 'MUKAH', 'district_id' => '1303'],
    ['id' => '130342', 'name' => 'GIGIS', 'district_id' => '1303'],
    ['id' => '130343', 'name' => 'SELANGAU', 'district_id' => '1303'],
    ['id' => '130344', 'name' => 'BALINGAN', 'district_id' => '1303'],
    ['id' => '130345', 'name' => 'ARIP', 'district_id' => '1303'],
    ['id' => '130346', 'name' => 'PELUNGAU', 'district_id' => '1303'],
    ['id' => '130347', 'name' => 'BAWAN', 'district_id' => '1303'],
    ['id' => '130348', 'name' => 'BULOH', 'district_id' => '1303'],
    ['id' => '130349', 'name' => 'BANDAR SIBU', 'district_id' => '1303'],
    ['id' => '130350', 'name' => 'BANDAR SENGEI MERAH', 'district_id' => '1303'],
    ['id' => '130351', 'name' => 'BANDAR TEKU', 'district_id' => '1303'],
    ['id' => '130352', 'name' => 'BANDAR DURIN', 'district_id' => '1303'],
    ['id' => '130360', 'name' => 'BANDAR KANOWIT', 'district_id' => '1303'],
    ['id' => '130363', 'name' => 'BANDAR DAP', 'district_id' => '1303'],
    ['id' => '130365', 'name' => 'BANDAR MACHAN', 'district_id' => '1303'],
    ['id' => '130366', 'name' => 'BANDAR NGEMAH', 'district_id' => '1303'],
    ['id' => '130368', 'name' => 'BANDAR SENGAYAN', 'district_id' => '1303'],
    ['id' => '130380', 'name' => 'BANDAR SIBINTEK', 'district_id' => '1303'],
    /*
     * === MIRI
     */
    ['id' => '130401', 'name' => 'KONSESI MIRI', 'district_id' => '1304'],
    ['id' => '130402', 'name' => 'BANDAR LUTONG', 'district_id' => '1304'],
    ['id' => '130403', 'name' => 'BANDAR BAZAR JALAN RIAM', 'district_id' => '1304'],
    ['id' => '130404', 'name' => 'KUALA BARAM', 'district_id' => '1304'],
    ['id' => '130407', 'name' => 'LAMBIR', 'district_id' => '1304'],
    ['id' => '130449', 'name' => 'BANDAR KUALA NYABOR', 'district_id' => '1304'],
    ['id' => '130450', 'name' => 'BAREO', 'district_id' => '1304'],
    ['id' => '130452', 'name' => 'APOH', 'district_id' => '1304'],
    ['id' => '130453', 'name' => 'LIO MATOH', 'district_id' => '1304'],
    ['id' => '130456', 'name' => 'SILAT', 'district_id' => '1304'],
    ['id' => '130457', 'name' => 'TUTOH', 'district_id' => '1304'],
    ['id' => '130458', 'name' => 'PATAH', 'district_id' => '1304'],
    ['id' => '130459', 'name' => 'LEPU LEJU', 'district_id' => '1304'],
    /*
     * === LIMBANG
     */
    ['id' => '130501', 'name' => 'DANAU', 'district_id' => '1305'],
    ['id' => '130502', 'name' => 'PANARUAN', 'district_id' => '1305'],
    ['id' => '130503', 'name' => 'TRUSAN', 'district_id' => '1305'],
    ['id' => '130504', 'name' => 'LAWAS', 'district_id' => '1305'],
    ['id' => '130505', 'name' => 'MERAPOK', 'district_id' => '1305'],
    ['id' => '130506', 'name' => 'LIMBANG', 'district_id' => '1305'],
    ['id' => '130507', 'name' => 'DANAU', 'district_id' => '1305'],
    ['id' => '130508', 'name' => 'NANGA MEDAMIT', 'district_id' => '1305'],
    ['id' => '130509', 'name' => 'TRUSAN', 'district_id' => '1305'],
    ['id' => '130510', 'name' => 'LAWAS', 'district_id' => '1305'],
    ['id' => '130511', 'name' => 'MERAPOK', 'district_id' => '1305'],
    ['id' => '130512', 'name' => 'UKONG', 'district_id' => '1305'],
    ['id' => '130513', 'name' => 'BANDAR SUNDAR', 'district_id' => '1305'],
    ['id' => '130514', 'name' => 'SUNGAI ADANG', 'district_id' => '1305'],
    ['id' => '130515', 'name' => 'LONG NAPIR', 'district_id' => '1305'],
    ['id' => '130516', 'name' => 'SUNGAI ADDANG', 'district_id' => '1305'],
    ['id' => '130517', 'name' => 'TENGOA-SUKANG', 'district_id' => '1305'],
    ['id' => '130518', 'name' => 'LONG NERAPAP', 'district_id' => '1305'],
    ['id' => '130519', 'name' => 'LONG SEMADO', 'district_id' => '1305'],
    ['id' => '130520', 'name' => 'BAKALALAN', 'district_id' => '1305'],
    ['id' => '130521', 'name' => 'BATU LAWI', 'district_id' => '1305'],
    /*
     * === SARIKEI
     */
    ['id' => '130607', 'name' => 'SERENDANG', 'district_id' => '1306'],
    ['id' => '130610', 'name' => 'MARADONG', 'district_id' => '1306'],
    ['id' => '130611', 'name' => 'TULAI', 'district_id' => '1306'],
    ['id' => '130612', 'name' => 'SARIKEI', 'district_id' => '1306'],
    ['id' => '130614', 'name' => 'BUAN', 'district_id' => '1306'],
    ['id' => '130615', 'name' => 'SARE', 'district_id' => '1306'],
    ['id' => '130616', 'name' => 'PEDANUM', 'district_id' => '1306'],
    ['id' => '130617', 'name' => 'MELURUN', 'district_id' => '1306'],
    ['id' => '130618', 'name' => 'JIKANG', 'district_id' => '1306'],
    ['id' => '130654', 'name' => 'BANDAR BINATANG', 'district_id' => '1306'],
    ['id' => '130661', 'name' => 'JULAU', 'district_id' => '1306'],
    ['id' => '130662', 'name' => 'BANDAR PAKAN', 'district_id' => '1306'],
    ['id' => '130674', 'name' => 'BANDAR SELALANG', 'district_id' => '1306'],
    ['id' => '130677', 'name' => 'GUNUNG AYER', 'district_id' => '1306'],
    ['id' => '130692', 'name' => 'BINATANG', 'district_id' => '1306'],
    /*
     * === KAPIT
     */
    ['id' => '130723', 'name' => 'KATIBAS', 'district_id' => '1307'],
    ['id' => '130724', 'name' => 'IBAU', 'district_id' => '1307'],
    ['id' => '130725', 'name' => 'MENUAN', 'district_id' => '1307'],
    ['id' => '130726', 'name' => 'SUAU', 'district_id' => '1307'],
    ['id' => '130727', 'name' => 'OYAN', 'district_id' => '1307'],
    ['id' => '130728', 'name' => 'BANING', 'district_id' => '1307'],
    ['id' => '130729', 'name' => 'MAJAU', 'district_id' => '1307'],
    ['id' => '130730', 'name' => 'MENRAL', 'district_id' => '1307'],
    ['id' => '130731', 'name' => 'METAH', 'district_id' => '1307'],
    ['id' => '130732', 'name' => 'RIRONG', 'district_id' => '1307'],
    ['id' => '130733', 'name' => 'MAMU', 'district_id' => '1307'],
    ['id' => '130735', 'name' => 'ANGKUAH', 'district_id' => '1307'],
    ['id' => '130736', 'name' => 'PELAGUS', 'district_id' => '1307'],
    ['id' => '130769', 'name' => 'BANDAR KAPIT', 'district_id' => '1307'],
    ['id' => '130770', 'name' => 'BANDAR SONG', 'district_id' => '1307'],
    ['id' => '130781', 'name' => 'BANGKIT', 'district_id' => '1307'],
    ['id' => '130782', 'name' => 'BATU LAGA', 'district_id' => '1307'],
    ['id' => '130783', 'name' => 'PELANDUK', 'district_id' => '1307'],
    ['id' => '130784', 'name' => 'ENTEMU', 'district_id' => '1307'],
    ['id' => '130785', 'name' => 'MENGIONG', 'district_id' => '1307'],
    ['id' => '130786', 'name' => 'SERANI', 'district_id' => '1307'],
    ['id' => '130787', 'name' => 'BALUI', 'district_id' => '1307'],
    ['id' => '130788', 'name' => 'KUMBONG', 'district_id' => '1307'],
    ['id' => '130789', 'name' => 'MURUM', 'district_id' => '1307'],
    ['id' => '130790', 'name' => 'PUNAN', 'district_id' => '1307'],
    ['id' => '130791', 'name' => 'DANUM', 'district_id' => '1307'],
    /*
     * === SAMARAHAN
     */
    ['id' => '130810', 'name' => 'LESONG', 'district_id' => '1308'],
    ['id' => '130814', 'name' => 'MENUKU', 'district_id' => '1308'],
    ['id' => '130815', 'name' => 'KAYAN', 'district_id' => '1308'],
    ['id' => '130816', 'name' => 'SAMARAHAN', 'district_id' => '1308'],
    ['id' => '130817', 'name' => 'MUARA TUANG', 'district_id' => '1308'],
    ['id' => '130818', 'name' => 'BUKAR-SADONG', 'district_id' => '1308'],
    ['id' => '130819', 'name' => 'SUNGAI KEDUP', 'district_id' => '1308'],
    ['id' => '130820', 'name' => 'MELIKIN LAND', 'district_id' => '1308'],
    ['id' => '130821', 'name' => 'SEDILU-GEDONG', 'district_id' => '1308'],
    ['id' => '130822', 'name' => 'SADONG', 'district_id' => '1308'],
    ['id' => '130823', 'name' => 'SEBANGAN-KEPAYAN', 'district_id' => '1308'],
    ['id' => '130824', 'name' => 'PUNDA-SABAL', 'district_id' => '1308'],
    ['id' => '130826', 'name' => 'BANDAR SEBUYAU', 'district_id' => '1308'],
    ['id' => '130835', 'name' => 'BANDAR SUNGAI MERAH', 'district_id' => '1308'],
    ['id' => '130836', 'name' => 'BANDAR SUNAGAI MERANG', 'district_id' => '1308'],
    ['id' => '130837', 'name' => 'BANDAR SUNGAI PALAH', 'district_id' => '1308'],
    ['id' => '130839', 'name' => 'BATU 29, JALAN SIMANGGANG', 'district_id' => '1308'],
    ['id' => '130840', 'name' => 'BATU 32, JALAN SIMANGGANG', 'district_id' => '1308'],
    ['id' => '130841', 'name' => 'BATU 34+, JALAN SIMANGGANG', 'district_id' => '1308'],
    ['id' => '130842', 'name' => 'BANDAR SERIAN', 'district_id' => '1308'],
    ['id' => '130843', 'name' => 'BANDAR TEBAKANG', 'district_id' => '1308'],
    ['id' => '130844', 'name' => 'BANDAR MUARA MONGKOS', 'district_id' => '1308'],
    ['id' => '130845', 'name' => 'BANDAR TEDEBU', 'district_id' => '1308'],
    ['id' => '130846', 'name' => 'BANDAR BALAI RINGIN', 'district_id' => '1308'],
    ['id' => '130847', 'name' => 'BANDAR TAMBIRAT', 'district_id' => '1308'],
    ['id' => '130848', 'name' => 'BANDAR MUARA TUANG', 'district_id' => '1308'],
    ['id' => '130852', 'name' => 'BANDAR SIMUJAN', 'district_id' => '1308'],
    ['id' => '130853', 'name' => 'BANDAR GEDONG', 'district_id' => '1308'],
    ['id' => '130854', 'name' => 'BANDAR SEBANGAN', 'district_id' => '1308'],
    ['id' => '130865', 'name' => 'BANDAR TEBELU', 'district_id' => '1308'],
    ['id' => '130877', 'name' => 'BANDAR TERBAT', 'district_id' => '1308'],
    /*
     * === BINTULU 
     */
    ['id' => '130931', 'name' => 'BINTULU', 'district_id' => '1309'],
    ['id' => '130932', 'name' => 'KEMENA', 'district_id' => '1309'],
    ['id' => '130933', 'name' => 'SEBAUH', 'district_id' => '1309'],
    ['id' => '130934', 'name' => 'BANDAR LANANG', 'district_id' => '1309'],
    ['id' => '130935', 'name' => 'BANDAR PANDAN', 'district_id' => '1309'],
    ['id' => '130936', 'name' => 'BANDAR TUBAU', 'district_id' => '1309'],
    ['id' => '130938', 'name' => 'SELEZU', 'district_id' => '1309'],
    ['id' => '130939', 'name' => 'BATU KAPAL', 'district_id' => '1309'],
    ['id' => '130940', 'name' => 'RASAN', 'district_id' => '1309'],
    ['id' => '130941', 'name' => 'PANDAN', 'district_id' => '1309'],
    ['id' => '130942', 'name' => 'BANDAR KUALA TATAU', 'district_id' => '1309'],
    ['id' => '130943', 'name' => 'BANDAR TATAU', 'district_id' => '1309'],
    ['id' => '130945', 'name' => 'BUAN LAN', 'district_id' => '1309'],
    ['id' => '130946', 'name' => 'SANGAN', 'district_id' => '1309'],
    ['id' => '130947', 'name' => 'ANAP', 'district_id' => '1309'],
    ['id' => '130948', 'name' => 'KAKUS', 'district_id' => '1309'],
    ['id' => '130954', 'name' => 'JELALONG', 'district_id' => '1309'],
    ['id' => '130955', 'name' => 'BINIA', 'district_id' => '1309'],
    /*
     * === MUKAH
     */
    /*
     * === BETONG
     */
    ['id' => '131146', 'name' => 'BANDAR MALUDAM', 'district_id' => '1311'],
    
    /*
     * LUAR NEGARA
     */
    ['id' => '980101', 'name' => '-', 'district_id' => '9801'],
];
