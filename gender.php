 <?php
/**
 * @author    syafiq <syafiq@my-sands.com>
 * @since     1.1.0 build 43ec40
 * @version   1.1.0 build 43ec40
 * @copyright 2015 syafiq
 * @filesource
 */
/**
 * gender - jantina.
 * sddsa : Kategori Data > Data Generik > Biodata > Kod Jantina
 * code_1 - sddsa
 */
$items = [
    [ 'id' => 1, 'name' => 'Lelaki',    'code_1' => 'L' ],
    [ 'id' => 2, 'name' => 'Perempuan', 'code_1' => 'P' ],
    [ 'id' => 3, 'name' => 'Ragu',      'code_1' => 'R' ] 
];
