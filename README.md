jajahan
=======

intro

v1.0 - Jajahan adalah senarai alternatif untuk negeri, daerah, mukim, dun, bahagian, parlimen di dalam Malaysia. Tujuan asal adalah untuk digunakan di dalam sistem sebagai fix data yang tidak perlu masuk ke database kerana pertukarannya amat sedikit mengikut masa.

v1.1 - jajahan dilanjutkan kepada beberapa data asas yang diguna pakai didalam malaysia. Data SDDSA tidak stabil kerapa ada pengulangan data yang menyebabkan kemungkinan data disatu sektor berlainan dengan sektor yang lain, maka fail baharu akan diletakkan sekali breadcrumb kepada data tersebut.

sumber data

sddsa.mampu.gov.my - data tak ada link dengan antara entiti
statistics.gov.my - tidak lengkap dan out of date

todo

mukim: sabah, sarawak
bahagian: untuk sabah, sarawak
tambah field active untuk manage data yang akan/telah berubah. e.g subdistrict : bukit serampang tukar daripada muar kepada ledang
